/*
 * Copyright (C) 2019 Brian Haslett
 *    knotwurk@gmail.com
 *
 * maps and fills all available memory
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf, getline, scanf
#include <stdlib.h>   // for exit
#include <sys/mman.h> // for mlock, munlock
#include <string.h>   // for strstr
#include <ctype.h>    // for isdigit
#include <sys/random.h>   // for getrandom
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <stdbool.h>

struct data_s;
size_t get_free_bytes();
void scrub_mem(struct data_s *);
void print_help();

static struct option long_options[] = {
  { "zero", 0, NULL, 'z' },
  { "silent", 0, NULL, 's' },
  { "random", 0, NULL, 'r' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

struct data_s {
  bool sel_silent, sel_random, sel_zero;
  size_t len;
};

int main(int argc, char *argv[])
{
  int opt;
  struct data_s data = {
    .sel_silent=false,
    .sel_random=false,
    .sel_zero=false
  };

  if (argc < 2) {
    print_help();
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "zsrh", long_options, NULL)) != -1) {
    switch (opt) {
      case 'z':
        data.sel_zero=true;
        break;
      case 's':
        data.sel_silent=true;
        break;
      case 'r':
        data.sel_random=true;
        break;
      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
        break;
      default:
        print_help();
        return 0;
    }
  }
    
  if (data.sel_zero == true || data.sel_random == true) {
    system("echo 3 > /proc/sys/vm/drop_caches");
    data.len = get_free_bytes();
    scrub_mem(&data);
  } else {
    print_help();
    exit(EXIT_FAILURE);
  }

  return 0;
}

size_t get_free_bytes()
{
  short i=0, j=0;
  size_t n=0, len;
  char *buf=NULL, *out_buf = NULL;
  FILE *fp = fopen("/proc/meminfo", "r");
  
  while (getline(&buf, &n, fp) != -1) {
    if ((strstr(buf, "MemAvailable") != NULL)) {
      while (buf[i] != '\n') {
        if (isdigit(buf[i])) {
          j++;
          out_buf = realloc(out_buf, j);
          out_buf[j-1] = buf[i];
        }
        i++;
      }    
    }
  }

  sscanf(out_buf, "%zd", &len);

  /*
   * https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-meminfo
   */
  len *= 1024;

  free(buf);
  free(out_buf);
  fclose(fp);
  return len;
}

void scrub_mem(struct data_s *data)
{
  void *addr = NULL;
  int retval = 0, len_count = 0;

  if (data->sel_silent == false)
    printf("%u free bytes\n", data->len);

  addr = malloc(data->len);
  if (!addr) {
    fprintf(stderr, "Failed to allocate memory (scrub_mem)\n");
    exit(EXIT_FAILURE);
  }

  mlock(addr,data->len);
  if (data->sel_random == true) {
    while (retval >= 0 && retval != data->len) {
      retval += getrandom(addr+retval,data->len-retval,GRND_NONBLOCK);
      if (data->sel_silent == false)
        printf("%d pseudo-random bytes written, len=%zd\n", retval, data->len);
    }
  } else {
    memset(addr,0,data->len);
  }
  munlock(addr,data->len);

  if (data->sel_silent == false)
    printf("Memory scrubbed at %p\n", addr);

  free(addr);
}

void print_help()
{
  puts("scrub-free");
  puts("scrub free memory\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -s, --silent\t\tno output");
  puts("  -r, --random\t\tfill with pseudo-random data");
  puts("  -z, --zero\t\tfill with zeroes\n");
  puts("Either the zero or random option is required");
}
