/*
 * Copyright (C) 2019-2021 Brian Haslett
 *
 * exercise, sort a set of numbers using a binary heap, binary sort tree, or
 * something along those lines
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf
#include <string.h>   // for strlen
#include <stdlib.h>     // for exit
#include <getopt.h>     // for getopt, getopt_long, struct option
#include <stdbool.h>    // for bools
#include <errno.h>

static struct option long_options[] = {
  { "input", 1, NULL, 'i' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

enum flags_e {
  TOP,
  LEFT,
  RIGHT
} flags;

struct data_s {
  int flag;
  unsigned int x;
  struct data_s *Parent;
  struct data_s *ChildA;
  struct data_s *ChildB;
};
struct raw_data_s {
  unsigned int input_len;
  unsigned char *input;
};

void print_help();
int parse_input(char*, struct raw_data_s*);
void init_tree(struct data_s*, struct raw_data_s*);
void build_tree(struct data_s*, unsigned int*, int);
void sort_data(struct data_s*);
void print_data(struct data_s*);

int main(int argc, char *argv[])
{
  bool has_input=false;
  int opt, rv;
  struct data_s *data = malloc(sizeof(*data));
  struct raw_data_s *rdata = malloc(sizeof(*rdata));

  if (argc < 2) {
    print_help();
    free(data);
    free(rdata);
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "i:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'i':
        rv = parse_input(optarg, rdata);
        if (rv == 0) {
          has_input=true;
        }
        break;
      case 'h':
        print_help();
        free(data);
        free(rdata);
        exit(EXIT_SUCCESS);
      default:
        print_help();
        free(data);
        free(rdata);
        return 0;
    }
  }

  if (has_input == true) {
    init_tree(data, rdata);
    print_data(data);
  }

end:
  free(data);
  free(rdata);
  return 0;
}

/*
 * parses user input
 *
 * returns 0 upon success, error upon failure
 */
int parse_input(char *optarg, struct raw_data_s *rdata)
{
  int msg_count = 0, c;

  if (!strcmp(optarg,"-")) {  // read from stdin
    while ((c = getchar()) != EOF) {
      msg_count++;

      rdata->input = realloc(rdata->input,msg_count+1);
      if (!rdata->input) {
        fprintf(stderr, "Error allocating memory in main (msg_str/stdin)\n");
        return -ENOMEM;
      }

      rdata->input[msg_count-1] = c;
    }
    rdata->input[msg_count] = '\0';
    rdata->input_len=msg_count;

  } else {    // read from commandline options
    rdata->input_len=strlen(optarg);

    rdata->input = malloc(rdata->input_len+1);
    rdata->input[rdata->input_len]='\0';
    if (!rdata->input) {
      fprintf(stderr, "Error allocating memory in main (msg_str)\n");
      return -ENOMEM;
    }

    memcpy(rdata->input, optarg, rdata->input_len);

  }
  return 0;
}

/*
 * initialize and build the binary tree
 */
void init_tree(struct data_s *data, struct raw_data_s *rdata)
{
  int word_cnt, fields=0;
  unsigned int *cluster=malloc(1);
  unsigned char *hold = malloc(rdata->input_len+1);
  struct data_s *iterator = NULL;

  /*
   * copy user input from a string to an array of ints
   * the number of ints is stored as 'fields'
   */
  for (int tot_cnt=0; tot_cnt < rdata->input_len; tot_cnt++) {
    word_cnt=0;
    memset(hold, '\0', rdata->input_len);

    while (rdata->input[tot_cnt] != ' ' && tot_cnt < rdata->input_len) {
      memcpy(&hold[word_cnt], &rdata->input[tot_cnt], 1);
      word_cnt++;
      tot_cnt++;
    }
    fields++;

    cluster=realloc(cluster, (sizeof(int) * fields) +1);
    sscanf(hold, "%u", &cluster[fields-1]);
  }

  /*
   * we have an array of ints, now start building
   */
  data->Parent=NULL;
  for (int field_cnt=0; field_cnt<fields; field_cnt++) {
    if (data->Parent == NULL) {
      /* initializing */
      if (data->x == 0) {
        data->x = cluster[field_cnt];
        data->flag = TOP;
        continue;
      } else if (data->ChildA == NULL) {
        data->ChildA = malloc(sizeof(*data));
        data->ChildA->Parent = data;
        data->ChildA->flag=LEFT;
        data->ChildA->x=cluster[field_cnt];
        iterator = data->ChildA;
        continue;
      } else if (data->ChildB == NULL) {
        data->ChildB = malloc(sizeof(*data));
        data->ChildB->Parent = data;
        data->ChildB->flag = RIGHT;
        data->ChildB->x=cluster[field_cnt];
        continue;
      }
    }
    build_tree(iterator, cluster, field_cnt);
  }
}

/*
 * builds a binary tree then sorts it
 */
void build_tree(struct data_s *data, unsigned int *cluster, int field_cnt)
{
    if (data->ChildA == NULL) {
      data->ChildA = malloc(sizeof(*data));
      data->ChildA->x=cluster[field_cnt];
      data->ChildA->Parent = data;
      data->ChildA->flag = LEFT;
    } else if (data->ChildB == NULL) {
      data->ChildB = malloc(sizeof(*data));
      data->ChildB->x=cluster[field_cnt];
      data->ChildB->Parent = data;
      data->ChildB->flag = RIGHT;
    } else {
      data = data->ChildA;
      build_tree(data, cluster, field_cnt);
    }
    sort_data(data);
}

/*
 * This recursive tree sort consists of five basic tests:
 *
 * 1) Current & ChildA
 * 2) Current & ChildB
 * 3) ChildA & ChildB
 * 4) Current & Parent
 * 5) Current & Sibling
 */
void sort_data(struct data_s *data)
{
  unsigned int swapper;
  struct data_s *iterator = data;

  /* swap up from bottom-left */
  if (iterator->ChildA) {
    if (iterator->ChildA->x > iterator->x) {
      swapper = iterator->x;
      iterator->x = iterator->ChildA->x;
      iterator->ChildA->x = swapper;

      return sort_data(iterator);
    }
  }
  /* swap up from bottom-right */
  if (iterator->ChildB) {
    if (iterator->ChildB->x > iterator->x) {
      swapper = iterator->x;
      iterator->x = iterator->ChildB->x;
      iterator->ChildB->x = swapper;

      return sort_data(iterator);
    }
  }
  /* swap bottom-left and bottom-right */
  if (iterator->ChildA && iterator->ChildB) {
    if (iterator->ChildA->x > iterator->ChildB->x) {
      swapper = iterator->ChildA->x;
      iterator->ChildA->x = iterator->ChildB->x;
      iterator->ChildB->x = swapper;

      return sort_data(iterator);
    }
  }
  if (iterator->Parent) {
    /* swap up and iterate */
    if (iterator->Parent->x < iterator->x) {
      swapper = iterator->x;
      iterator->x = iterator->Parent->x;
      iterator->Parent->x = swapper;
      iterator = iterator->Parent;
      return sort_data(iterator);
    }
    /* swap right */
    if (iterator->flag == LEFT && iterator->Parent->ChildB) {
      if (iterator->x > iterator->Parent->ChildB->x) {
        swapper = iterator->x;
        iterator->x = iterator->Parent->ChildB->x;
        iterator->Parent->ChildB->x = swapper;

        return sort_data(iterator);
      }
    }
  }
}

void print_data(struct data_s *data)
{
  struct data_s *iterator = data;

  while (iterator->ChildA != NULL)
    iterator = iterator->ChildA;

  printf("%u ", iterator->x);
  iterator = iterator->Parent;
  if (iterator->ChildB)
    printf("%u ", iterator->ChildB->x);
  while (iterator->Parent != NULL) {
    printf("%u %u ", iterator->x, iterator->Parent->ChildB->x);
    iterator = iterator->Parent;
  }
  printf("%u\n", iterator->x);
}

void print_help()
{
  puts("sorter");
  puts("sorts a set of digits\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -i, --input[=INPUT]\tdigits to sort (whitespace separated)\n\t\t\tuse - for stdin");
}
