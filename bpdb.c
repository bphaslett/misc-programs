/*
 * Copyright (C) 2019-2021 Brian Haslett
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


/*
 * bpdb 2.0 format sepcifies TAB separated fields per line
 * lines can have any number of fields, with fields consisting of any ascii
 * character other than a tab or newline (afaik..)
 *
 * database format examples:
 *   foo	20	m
 *   bar	22	f
 *   pat	32
 *   jenny	867-5309	jlo@mysite.rules
 *   bob	555-1212	bobert@gotroot.now
 *   fredo	root@localhost
 *   This is one field	This is another.
 *
 * the new menu system is pretty user friendly, and while testing shows it to
 * be fairly stable, keeping a backup of anything important is still
 * recommended
 * you can start a database out from scratch by just specifying a filename and
 * adding an entry, the file is saved automatically
 *
 * an encryption scheme (multipass3) is supported, which requires setting a
 * passphrase and how many rounds of encryption
 * you can work directly with an encrypted database, which writes out the
 * encrypted form to disk when changes are made
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>     // for realloc
#include <string.h>     // for memset, strstr
#include <stdbool.h>
#include <sys/mman.h>   // for mmap/munmap
#include <errno.h>

#include "bpdb_crypt.h"
#include "bpdb.h"

char *filename;

int main(int argc, char *argv[])
{
  int rv;
  struct input_args ia = {};
  struct ll_node *node = malloc(sizeof(*node));
  struct shared_text shared = {};
  struct spiral sp = { .shared = &shared };
  struct vigenere vig = { .shared = &shared };
  struct railfence rail = { .shared = &shared };
  struct multipass multi = { 0, false, &shared, &vig, &rail, &sp };
  struct ll_common ll = { 0, &multi, node };

  MALLOC_CHECK(node);

  if (argc < 2) {
    print_help();
    exit(EXIT_FAILURE);
  }

  parse_args(&ia, argc, argv);

  if (!filename) {
    fprintf(stderr, "Error, no filename specified\n");
    if (ia.user_input != NULL)
      munmap(ia.user_input, ia.input_len+1);
    free(node);
    return EINVAL;
  }

main_menu:
  multi.shared->msg_len = get_file_len(&ia);
  if (ia.encrypted_file == true) {
    rv = get_crypt_info(&multi);
    if (rv != 0)
      goto done;
  }

  switch (ia.fop) {
    case NONE:
      ia.use_menu = true;
      ia.fop = display_menu(&ia);
      ia.input_len = parse_input_menu(&ia);
      goto main_menu;
      break;
    case DECRYPT:
      decrypt_file(&multi, &ia);

      ia.encrypted_file=false;
      free(node);
      break;
    case ENCRYPT:
      encrypt_file(&multi, &ia);

      ia.encrypted_file=true;
      free(node);
      break;
    case PRINT:
      if (ia.encrypted_file == true) {
        print_encrypted(&ll, &multi);
      } else {
        print_plain(&ll);
      }
      free_data(&ll);
      break;
    case SEARCH:
      if (ia.encrypted_file == true) {
        search_encrypted(&ia, &ll, &multi);
      } else {
        search_plain(&ia, &ll);
      }
      munmap(ia.user_input, ia.input_len+1);
      free_data(&ll);
      break;
    case ADD:
      if (ia.encrypted_file == true) {
        add_encrypted(&ia, &multi);
      } else {
        add_plain(&ia);
      }
      munmap(ia.user_input, ia.input_len+1);
      break;
    case DELETE:
      if (ia.encrypted_file == true) {
        delete_encrypted(&ia, &ll, &multi);
      } else {
        delete_plain(&ia, &ll);
      }
      munmap(ia.user_input, ia.input_len+1);
      free_data(&ll);
      break;
    case CHANGE:
      if (ia.encrypted_file == true) {
        change_encrypted(&ia, &ll, &multi);
      } else {
        change_plain(&ia, &ll);
      }
      munmap(ia.user_input, ia.input_len+1);
      free_data(&ll);
      break;
    case QUIT:
      goto done;
      break;
  }
  if (ia.use_menu == true) {
    if (ia.user_input)
      munmap(ia.user_input, ia.input_len+1);
    ia.fop = NONE;
    node = malloc(sizeof(*node));
    ll.node = node;
    goto main_menu;
  }

done:
  if (sizeof(multi.vig->key) == multi.vig->key_len+1)
    munmap(multi.vig->key, multi.vig->key_len+1);
  free(filename);
  return 0;
}

void parse_args(struct input_args *ia, int argc, char *argv[])
{
  int opt;
  while ((opt = getopt_long(argc, argv, "vEDed:a:s:c:pf:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'D':
        ia->encrypted_file=true;
        FOP_CHECK(ia->fop);
        ia->fop=DECRYPT;
        break;
      case 'E':
        ia->encrypted_file=false;
        FOP_CHECK(ia->fop);
        ia->fop=ENCRYPT;
        break;
      case 'e':
        ia->encrypted_file=true;
        break;
      case 'a':
        FOP_CHECK(ia->fop);
        ia->input_len = strlen(optarg);
        ia->user_input = mmap(NULL, ia->input_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
        MMAP_CHECK(ia->user_input);
        strcpy(ia->user_input, optarg);
        mprotect(ia->user_input, ia->input_len+1, PROT_READ);
        ia->fop=ADD;
        break;
      case 'd':
        FOP_CHECK(ia->fop);
        ia->input_len = strlen(optarg);
        ia->user_input = mmap(NULL, ia->input_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
        MMAP_CHECK(ia->user_input);
        strcpy(ia->user_input, optarg);
        mprotect(ia->user_input, ia->input_len+1, PROT_READ);
        ia->fop=DELETE;
        break;
      case 's':
        FOP_CHECK(ia->fop);
        ia->input_len = strlen(optarg);
        ia->user_input = mmap(NULL, ia->input_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
        MMAP_CHECK(ia->user_input);
        strcpy(ia->user_input, optarg);
        mprotect(ia->user_input, ia->input_len+1, PROT_READ);
        ia->fop=SEARCH;
        break;
      case 'c':
        FOP_CHECK(ia->fop);
        ia->input_len = strlen(optarg);
        ia->user_input = mmap(NULL, ia->input_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
        MMAP_CHECK(ia->user_input);
        strcpy(ia->user_input, optarg);
        mprotect(ia->user_input, ia->input_len+1, PROT_READ);
        ia->fop=CHANGE;
        break;
      case 'p':
        FOP_CHECK(ia->fop);
        ia->fop=PRINT;
        break;
      case 'f':
        filename = malloc(strlen(optarg)+1);
        MALLOC_CHECK(filename);
        strcpy(filename, optarg);
        break;
      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
        break;
      case 'v':
        print_version();
        exit(EXIT_SUCCESS);
        break;
      default:
        print_help();
        exit(EXIT_FAILURE);
        break;
    }
  }
}

enum fileops display_menu(struct input_args *ia)
{
  char *buf = NULL, sel;
  size_t buf_len;

  while (true) {
    puts("==============");
    puts("bpdb main menu");
    puts("==============");
    puts("");
    puts("(p)rint");
    puts("(s)earch");
    puts("(a)dd");
    puts("(d)elete");
    puts("(c)hange");
    puts("(E)ncrypt");
    puts("(D)ecrypt");
    puts("(q)uit");
    puts("");

    if (ia->encrypted_file == true) {
      printf("[ENCRYPTED] > ");
    } else {
      printf("[PLAINTEXT] > ");
    }
    parse_input(&buf);
    sel = buf[0];

    switch (sel) {
      case 'p':
        return PRINT;
        break;
      case 's':
        return SEARCH;
        break;
      case 'a':
        return ADD;
        break;
      case 'd':
        return DELETE;
        break;
      case 'c':
        return CHANGE;
        break;
      case 'E':
        return ENCRYPT;
        break;
      case 'D':
        return DECRYPT;
        break;
      case 'q':
        return QUIT;
        break;
    }

    munmap(buf, buf_len);
  }

}

size_t parse_input_menu(struct input_args *ia)
{
  switch (ia->fop) {
    case SEARCH:
      printf("Enter search query: ");
      break;
    case ADD:
      return parse_newline(&ia->user_input);
      break;
    case DELETE:
      printf("Enter deletion query: ");
      break;
    case CHANGE:
      printf("Enter change query: ");
      break;
    default:
      return 0;
      break;
  }

  return parse_input(&ia->user_input);
}

size_t get_file_len(struct input_args *ia)
{
  size_t msg_len;

  stat(filename, &ia->sb);
  msg_len = ia->sb.st_size;

  return msg_len;
}

size_t parse_newline(char **new_line)
{
  int fieldno;
  size_t map_len, input_len, total_len = 1;
  char *input_buf = NULL;
  char *tmp_buf;

  *new_line = mmap(NULL, total_len, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, 0, 0);
  MMAP_CHECK(*new_line);

  printf("Number of fields: ");
  input_len = parse_input(&input_buf);
  if (input_len == 0) {
    errno = EINVAL;
    return -1;
  }

  fieldno = atoi(input_buf);
  munmap(input_buf, input_len);
  for (int i=0; i<fieldno; i++) {
    printf("Enter field %d: ", i+1);
    map_len = parse_input(&input_buf);
    input_len=strlen(input_buf);
    if (input_len <= 0) {
      errno = EINVAL;
      return -1;
    }

    tmp_buf = mremap(*new_line, total_len, total_len+input_len+2, 0);
    MREMAP_CHECK(*new_line, total_len, tmp_buf);
    *new_line = tmp_buf;

    strcat(*new_line, input_buf);
    strcat(*new_line, "\t");

    total_len+=input_len+1;
    munmap(input_buf, map_len+1);
  }

  munmap(input_buf, input_len);
  return total_len;
}

void add_encrypted(struct input_args *ia, struct multipass *multi)
{
  int rv;
  char *buf = NULL;
  FILE *fp = NULL;
  char *tmp_buf = NULL;

  rv = decrypt_to_buf(multi, &buf);
  if (rv != 0)
    goto error;

  tmp_buf = mremap(buf, multi->shared->msg_len + 1, multi->shared->msg_len + ia->input_len + 2, 0);
  MREMAP_CHECK(buf, multi->shared->msg_len+1, tmp_buf);
  buf = tmp_buf;
  strcat(buf, ia->user_input);
  strcat(buf, "\n");

  rv = encrypt_mem(multi, buf);
  if (rv != 0)
    goto error;

  fp = fopen(filename, "w+");
  FP_CHECK(fp);
  fputs(buf, fp);
  munmap(buf, multi->shared->msg_len + strlen(buf) + ia->input_len+2);
  fclose(fp);
  return;

error:
  fprintf(stderr, "Error in add_encrypted: %s\n", strerror(errno));
  munmap(ia->user_input, ia->input_len+1);
  free(filename);
  exit(EXIT_FAILURE);
}
void add_plain(struct input_args *ia)
{
  FILE *fp = fopen(filename, "a+");
  FP_CHECK(fp);
  fprintf(fp, "%s\n", ia->user_input);
  fclose(fp);
}

void change_encrypted(struct input_args *ia, struct ll_common *ll, struct multipass *multi)
{
  int rv;

  rv = decrypt_to_mem(multi, ll);
  if (rv != 0)
    goto error;

  change_data(ll, ia);
  if (ia->update_file == true)
    write_encrypted(ll, multi);

  return;

error:
  fprintf(stderr, "Error in change_encrypted: %s\n", strerror(errno));
  free_data(ll);
  munmap(ia->user_input, ia->input_len+1);
  free(filename);
  exit(EXIT_FAILURE);
}
void change_plain(struct input_args *ia, struct ll_common *ll)
{
  FILE *fp = fopen(filename, "r");
  FP_CHECK(fp);
  init_data_from_file(ll, fp);
  fclose(fp);

  change_data(ll, ia);
  if (ia->update_file == true)
    write_plain(ll);
}

void delete_encrypted(struct input_args *ia, struct ll_common *ll, struct multipass *multi)
{
  int rv;

  rv = decrypt_to_mem(multi, ll);
  if (rv != 0)
    goto error;

  delete_data(ll, ia);
  if (ia->update_file == true)
    write_encrypted(ll, multi);

  return;

error:
  fprintf(stderr, "Error in delete_encrypted: %s\n", strerror(errno));
  free_data(ll);
  munmap(ia->user_input, ia->input_len+1);
  free(filename);
  exit(EXIT_FAILURE);
}
void delete_plain(struct input_args *ia, struct ll_common *ll)
{
  FILE *fp = fopen(filename, "r");
  FP_CHECK(fp);
  init_data_from_file(ll, fp);
  fclose(fp);

  delete_data(ll, ia);
  if (ia->update_file == true)
    write_plain(ll);
}

void search_encrypted(struct input_args *ia, struct ll_common *ll, struct multipass *multi)
{
  int rv;

  rv = decrypt_to_mem(multi, ll);
  if (rv != 0)
    goto error;

  search_data(ll, ia);

  return;

error:
  fprintf(stderr, "Error in search_encrypted: %s\n", strerror(errno));
  free_data(ll);
  munmap(ia->user_input, ia->input_len+1);
  free(filename);
  exit(EXIT_FAILURE);
}
void search_plain(struct input_args *ia, struct ll_common *ll)
{
  FILE *fp = fopen(filename, "r");
  FP_CHECK(fp);
  init_data_from_file(ll, fp);
  fclose(fp);

  search_data(ll, ia);
}

void print_encrypted(struct ll_common *ll, struct multipass *multi)
{
  int rv;

  rv = decrypt_to_mem(multi, ll);
  if (rv != 0)
    goto error;

  print_data(ll);

  return;

error:
  fprintf(stderr, "Error in print_encrypted: %s\n", strerror(errno));
  free_data(ll);
  free(filename);
  exit(EXIT_FAILURE);
}
void print_plain(struct ll_common *ll)
{
  FILE *fp = fopen(filename, "r");
  FP_CHECK(fp);
  init_data_from_file(ll, fp);
  fclose(fp);

  print_data(ll);
}

/*
 * initialize linked list from a file
 *
 * fp: pointer to file opened with read access
 * ll: the linked list where data is initialized
 */
void init_data_from_file(struct ll_common *ll, FILE *fp)
{
  int line_cnt, num_lines=0, chunks=0;
  size_t field_len=0, node_len = sizeof(*ll->node);
  char *line_buf = NULL, *data_buf = mmap(NULL, CHUNK, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  char *tmp_buf = NULL;

  MMAP_CHECK(data_buf);
  ll->head = ll->node;

  while (getline(&line_buf, &ll->multi->shared->msg_len, fp) != -1) {
    line_cnt = 0;

    ll->line_head = ll->node;
    ll->line_head->num_fields = 0;
    ll->line_head->delete = false;
    num_lines++;

    if (line_buf[0] == '\n')
      continue;

    while (line_buf[line_cnt] != '\n') {
      ll->line_head->num_fields++;
      field_len = 0;
      chunks = 1;

      /* read until delimiter or EOL */
      while (line_buf[line_cnt] != '\t' && line_buf[line_cnt] != '\n') {
          field_len++;
          if ((field_len+1 % CHUNK) == 0) {
            chunks++;
            tmp_buf = mremap(data_buf, CHUNK*(chunks-1), CHUNK*chunks, 0);
            MREMAP_CHECK(data_buf, CHUNK*(chunks-1), tmp_buf);
            data_buf = tmp_buf;
          }
          data_buf[field_len-1] = line_buf[line_cnt];
          line_cnt++;
      }
      data_buf[field_len] = '\0';

      ll->node->data_len=field_len;
      ll->node->data = mmap(NULL, ll->node->data_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
      MMAP_CHECK(ll->node->data);
      strcpy(ll->node->data, data_buf);

      /* zero it out since we're reusing the buffer */
      memset(data_buf, '\0', CHUNK*chunks);

      ll->node->next = malloc(node_len);
      MALLOC_CHECK(ll->node->next);
      ll->node->next->prev = ll->node;
      ll->node = ll->node->next;

      /* ignore the field separator */
      while (line_buf[line_cnt] == '\t')
        line_cnt++;
    }
  }

  free(line_buf);
  munmap(data_buf, CHUNK*chunks);

  ll->num_lines = num_lines;

  ll->tail = ll->node;
  ll->node = ll->head;
}

/*
 * initialize linked list from a block of memory
 *
 * ll: pointer to struct where linked list is located
 * multi: struct where msg_str is located
 */
void init_data_from_mem(struct ll_common *ll, struct multipass *multi)
{
  int line_cnt, num_lines=0, offset=0;
  size_t field_len=0, node_len = sizeof(*ll->node);
  size_t line_len = 0;
  char *line_buf = NULL, *data_buf = NULL;
  char *msg = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK(msg);
  memcpy(msg, multi->shared->msg_str, multi->shared->msg_len);
  msg[multi->shared->msg_len] = '\0';

  ll->head = ll->node;

  while (offset < multi->shared->msg_len) {
    line_len = get_line_from_mem(&line_buf, msg, offset);

    /* blank lines are ignored */
    if (line_len == 0)
      continue;

    offset += line_len;

    line_cnt = 0;

    ll->line_head = ll->node;
    ll->line_head->num_fields = 0;
    ll->line_head->delete = false;
    num_lines++;

    while (line_buf[line_cnt] != '\n') {
      data_buf = mmap(NULL, line_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
      MMAP_CHECK(data_buf);
      ll->line_head->num_fields++;
      field_len = 0;

      /* read until delimiter or EOL */
      while (line_buf[line_cnt] != '\t' && line_buf[line_cnt] != '\n') {
          data_buf[field_len] = line_buf[line_cnt];
          field_len++;
          line_cnt++;
      }
      data_buf[field_len] = '\0';

      ll->node->data_len=field_len;
      ll->node->data = mmap(NULL, ll->node->data_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
      MMAP_CHECK(ll->node->data);
      strcpy(ll->node->data, data_buf);
      munmap(data_buf, line_len+1);

      ll->node->next = malloc(node_len);
      MALLOC_CHECK(ll->node->next);
      ll->node->next->prev = ll->node;
      ll->node = ll->node->next;

      /* ignore the field separators */
      while (line_buf[line_cnt] == '\t')
        line_cnt++;
    }
  }

  munmap(line_buf, line_len+1);

  ll->num_lines = num_lines;

  ll->tail = ll->node;
  ll->node = ll->head;
}

/*
 * works sorta like getline
 * matches a string from a block of memory starting a given offset
 *
 * msg: char pointer containing string to be searched

 * out: pointer to char pointer where result is stored
 * must be preallocated, otherwise if *out is NULL,
 * a buffer will be allocated which must be freed afterward
 *
 * offset: offset of msg (so this can be called from a loop)
 *
 * returns the size of the line matched, relative to offset, including the newline
 */
size_t get_line_from_mem(char **out, char *msg, int offset)
{
  int line_cnt=0;
  void *tmp = NULL;
  size_t len;

  /* blank lines */
  if ((tmp = strchr(msg+offset, '\n')) == NULL)
    return 0;

  len = tmp - ((void*)msg+offset) + 1;

  if (*out == NULL) {
    *out = mmap(NULL, len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
    MMAP_CHECK(*out);
  }

  memcpy(*out, msg+offset, len);
  (*out)[len] = '\0';

  return len;
}

/*
 * searches a linked list for a string,
 * prints out all matches
 */
void search_data(struct ll_common *ll, struct input_args *ia)
{
  /* parse lines */
  for (int lines=0; lines < ll->num_lines; lines++) {
    ll->line_head = ll->node;

    /* parse fields */
    for (int fields=0; fields < ll->line_head->num_fields; fields++) {
      if (strstr(ll->node->data, ia->user_input) != NULL) {
        ll->node = ll->line_head;
        parse_line(ll, true, false);
        break;
      }
      if (ll->node == ll->tail)
        break;
      ll->node = ll->node->next;
    }
    if (ll->node == ll->tail)
      break;
  }

  ll->node = ll->head;
}

/*
 * parse user input
 *
 * initialize a char* to NULL then pass it by reference
 * var must be freed later with munmap, and should not be reused
 *
 * returns length of the mapping, NOT the string
 */
size_t parse_input(char **input)
{
  int c, chunks = 1;
  size_t msg_count = 0;
  char *tmp_buf = NULL;

  *input = mmap(*input, CHUNK, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  MMAP_CHECK(*input);
  while ((c = getchar()) != EOF && c != '\n') {
    msg_count++;

    if ((msg_count+1 % CHUNK) == 0) {
      chunks++;
      tmp_buf = mremap(*input, CHUNK*(chunks-1), CHUNK*chunks, 0);
      MREMAP_CHECK(*input, CHUNK*(chunks-1), tmp_buf);
      *input = tmp_buf;
    }

    (*input)[msg_count-1] = c;
  }
  if (msg_count == 0)
    return msg_count;

  (*input)[msg_count] = '\0';

  return CHUNK*chunks;
}

/*
 * deletes a line from the table
 *
 * ll: pointer to struct where db is stored in a linked list
 * ia: pointer to struct containing input arguments
 */
void delete_data(struct ll_common *ll, struct input_args *ia)
{
  size_t buf_len;
  char *buf = NULL;

  for (int i=0; i < ll->num_lines; i++) {
    ll->line_head = ll->node;

    /* for each line, parse fields */
    for (int j=0; j < ll->line_head->num_fields; j++) {
      if (strstr(ll->node->data, ia->user_input) != NULL) {
        ll->node = ll->line_head;

        parse_line(ll, true, false);
        printf("Delete this line (y/n)? ");
        buf_len = parse_input(&buf);

        if (buf[0] == 'y') {
          ia->update_file = true;
          ll->line_head->delete = true;
        }

        munmap(buf, buf_len);
        break;
      }
      if (ll->node == ll->tail)
        break;

      ll->node = ll->node->next;
    }
  }

  ll->node = ll->head;
}

/*
 * change a field in the database
 *
 * ll: pointer to struct where db is stored in a linked list
 * ia: pointer to struct containing input arguments
 */
void change_data(struct ll_common *ll, struct input_args *ia)
{
  for (int i=0; i < ll->num_lines; i++) {
    ll->line_head = ll->node;

    /* for each line, parse fields */
    for (int j=0; j < ll->line_head->num_fields; j++) {
      if (strstr(ll->node->data, ia->user_input) != NULL) {
        ll->node = ll->line_head;
        replace_field(ll, ia);

        break;
      }
      if (ll->node == ll->tail)
        break;
      ll->node = ll->node->next;
    }
  }

  ll->node = ll->head;
}

/*
 * parses a line in the linked list
 *
 * behavior is somewhat polymorphic, depending on bools passed:
 * bool print: print the line out
 * bool reset: reset to line head afterwards
 */
void parse_line(struct ll_common *ll, bool print, bool reset)
{
  for (int i=0; i < ll->line_head->num_fields; i++) {
    if (print == true)
      printf("█ %*s █", (unsigned int) ll->node->data_len, ll->node->data);
    if (ll->node == ll->tail)
      break;
    ll->node = ll->node->next;
  }
  if (print == true)
    puts("");

  if (reset == true)
    ll->node = ll->line_head;
}

void replace_field(struct ll_common *ll, struct input_args *ia)
{
  int field_no;
  size_t input_len, map_len;
  char *buf=NULL;
  char *tmp_buf = NULL;

  parse_line(ll, true, true);

  printf("Change which field (1-%d)? ", ll->line_head->num_fields);

  map_len = parse_input(&buf);
  if (map_len <= 0) {
    goto done;
  } else {
    input_len = strlen(buf);
    ia->update_file = true;
  }
  buf[input_len] = '\0';

  field_no = atoi(buf);
  if (field_no > ll->line_head->num_fields || field_no <= 0)
    goto done;

  for (int i=0; i < ll->line_head->num_fields; i++) {
    if (i == field_no-1) {
      printf("Please enter new field: ");

      map_len = parse_input(&buf);
      if (map_len <= 0) {
        goto done;
      }
      input_len = strlen(buf);
      buf[input_len] = '\0';

      memset(ll->node->data, '\0', ll->node->data_len);

      tmp_buf = mremap(ll->node->data, ll->node->data_len+1, input_len+1, 0);
      MREMAP_CHECK(ll->node->data, ll->node->data_len, tmp_buf);
      ll->node->data = tmp_buf;
      memcpy(ll->node->data, buf, input_len);
      ll->node->data[input_len]='\0';
      ll->node->data_len = input_len;
      break;
    }
    if (ll->node == ll->tail)
      break;
    ll->node = ll->node->next;
  }
  munmap(buf, map_len);

done:
  ll->node = ll->line_head;
  parse_line(ll, false, false);
}

/*
 * takes the entire database (which is stored in a linked list)
 *
 * ll: pointer to struct where linked list is located
 * multi: pointer to struct where encryption data is stored
 */
void write_plain(struct ll_common *ll)
{
  size_t line_len = 0, db_len = 0;
  char *line = NULL;
  char *db = NULL;
  FILE *fp = NULL;

  db_len = get_db_len(ll);
  db = mmap(NULL, db_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  MMAP_CHECK(db);

  for (int i=0; i < ll->num_lines; i++) {
    ll->line_head = ll->node;
    line_len = get_line_len(ll);
    ll->node = ll->line_head;
    line = mmap(NULL, line_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
    MMAP_CHECK(line);

    for (int j=0; j < ll->line_head->num_fields; j++) {

      /* ignore deletions */
      if (ll->line_head->delete == true) {
        ll->node = ll->node->next;
        continue;
      }

      strcat(line, ll->node->data);

      if (j != ll->line_head->num_fields-1) {
        /* add delimiter between fields */
        strcat(line, "\t");
      } else {
        /* don't forget new line */
        strcat(line, "\n");
      }
      if (ll->node == ll->tail)
        break;
      ll->node = ll->node->next;
    }
    strcat(db, line);
    munmap(line, line_len+1);
  }

  ll->node = ll->head;

  fp = fopen(filename, "w+");
  FP_CHECK(fp);
  fputs(db, fp);
  munmap(db, db_len+1);
  fclose(fp);
}
/*
 * encrypted version of write_plain
 */
void write_encrypted(struct ll_common *ll, struct multipass *multi)
{
  int rv;
  size_t line_len = 0, db_len = 0;
  char *line = NULL;
  char *db = NULL;
  FILE *fp = NULL;

  db_len = get_db_len(ll);
  db = mmap(NULL, db_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  MMAP_CHECK(db);

  for (int i=0; i < ll->num_lines; i++) {
    ll->line_head = ll->node;
    line_len = get_line_len(ll);
    ll->node = ll->line_head;
    line = mmap(NULL, line_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
    MMAP_CHECK(line);

    for (int j=0; j < ll->line_head->num_fields; j++) {

      /* ignore deletions */
      if (ll->line_head->delete == true) {
        ll->node = ll->node->next;
        continue;
      }

      strcat(line, ll->node->data);

      if (j != ll->line_head->num_fields-1) {
        /* add delimiter between fields */
        strcat(line, "\t");
      } else {
        /* don't forget new line */
        strcat(line, "\n");
      }
      if (ll->node == ll->tail)
        break;
      ll->node = ll->node->next;
    }
    strcat(db, line);
    munmap(line, line_len+1);
  }

  ll->node = ll->head;

  rv = encrypt_mem(multi, db);
  if (rv != 0) {
    fprintf(stderr, "Error encrypting memory: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  fp = fopen(filename, "w+");
  FP_CHECK(fp);
  fputs(db, fp);
  munmap(db, db_len+1);
  fclose(fp);
}

/*
 * get the length of the whole databaes
 *
 * returns length
 */
size_t get_db_len(struct ll_common *ll)
{
  size_t db_len = 0;
  for (int i=0; i < ll->num_lines; i++, db_len++)
    db_len += get_line_len(ll);

  ll->node = ll->head;
  return db_len;
}

/*
 * get the length of a line in the table
 *
 * returns length
 */
size_t get_line_len(struct ll_common *ll)
{
  size_t line_len = 0;

  ll->line_head = ll->node;
  for (int j=0; j < ll->line_head->num_fields; j++) {
    /* ignore deletions */
    if (ll->line_head->delete == true) {
      ll->node = ll->node->next;
      continue;
    }

    line_len += ll->node->data_len + 1; // every field has either delimiter or newline
    if (ll->node == ll->tail)
      break;
    ll->node = ll->node->next;
  }

  return line_len;
}

void print_data(struct ll_common *ll)
{
  ll->node = ll->head;

  while (ll->node != ll->tail) {
    ll->line_head = ll->node;
    parse_line(ll, true, false);
  }

  ll->node = ll->head;
}

void free_data(struct ll_common *ll)
{
  struct ll_node *tmp;

  ll->node = ll->head;
  while (ll->node != ll->tail) {
    tmp = ll->node;
    ll->node = ll->node->next;
    munmap(tmp->data, tmp->data_len+1);
    free(tmp);
  }
}

void decrypt_file(struct multipass *multi, struct input_args *ia)
{
  int rv = 0;
  FILE *fp = NULL;

  if (ia->encrypted_file == false) {
    fprintf(stderr, "Error, file already decrypted\n");
    return;
  }

  fp = fopen(filename, "r+");
  FP_CHECK(fp);

  rv = decrypt_file_multi3(multi, fp);
  fclose(fp);
  if (rv != 0)
    goto error;

  return;

error:
  fprintf(stderr, "Error decrypting \"%s\": %s\n", filename, strerror(errno));
  exit(EXIT_FAILURE);
}

void encrypt_file(struct multipass *multi, struct input_args *ia)
{
  int rv = 0;
  FILE *fp = NULL;

  if (ia->encrypted_file == true) {
    fprintf(stderr, "Error, file already encrypted\n");
    return;
  }

  rv = set_crypt_info(multi);
  if (rv != 0)
    goto error;

  fp = fopen(filename, "r+");
  FP_CHECK(fp);

  rv = encrypt_file_multi3(multi, fp);
  fclose(fp);
  if (rv != 0)
    goto error;

  return;

error:
  fprintf(stderr, "Error decrypting \"%s\": %s\n", filename, strerror(errno));
  exit(EXIT_FAILURE);
}

/*
 * decrypt multipass string to a buffer
 *
 * first initialize a char* to NULL then pass by ref
 * buf must be freed afterwards
 *
 * returns 0 on success or else errno
 */
int decrypt_to_buf(struct multipass *multi, char **buf)
{
  int rv = 0;
  FILE *fp = NULL;

  fp = fopen(filename, "r");
  FP_CHECK(fp);

  rv = decrypt_mem(multi, fp);
  fclose(fp);
  if (rv != 0) {
    return rv;
  }

  multi->shared->msg_len = strlen(multi->shared->msg_str);

  *buf = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  MMAP_CHECK(*buf);
  memcpy(*buf, multi->shared->msg_str, multi->shared->msg_len);
  multi->shared->msg_str[multi->shared->msg_len] = '\0';

  munmap(multi->shared->msg_str, multi->shared->msg_len+1);

	return 0;
}

/*
 * decrypt file to memory
 *
 * data is decrypted to a doubly linked list
 */
int decrypt_to_mem(struct multipass *multi, struct ll_common *ll)
{
  int rv = 0;
  FILE *fp = NULL;

  fp = fopen(filename, "r");
  FP_CHECK(fp);

  rv = decrypt_mem(multi, fp);
  fclose(fp);
  if (rv != 0) {
    return rv;
  }
  multi->shared->msg_len = strlen(multi->shared->msg_str);
  init_data_from_mem(ll, multi);

  munmap(multi->shared->msg_str, multi->shared->msg_len+1);

  return 0;
}

void print_help()
{
    puts("bpdb");
    puts("brian's primitive database");
    puts("utilizing bpdb format 2.0 (tab delimiters)\n");
    puts("Usage: [options]\n");
    puts("Options:");
    puts("  -e, --encrypted\tdatabase is encrypted (multipass3)");
    puts("  -f, --file[=FILENAME]\tset the FILENAME (required)");
    puts("  -h, --help\t\tyou are here");
    puts("  -v, --version\t\tprint version");
    puts("File Operations (required, one only):");
    puts("  -E, --encrypt\t\tencrypt the database (multipass3)");
    puts("  -D, --decrypt\t\tdecrypt the database (multipass3)");
    puts("  -p, --print\t\tprint the database out");
    puts("  -s, --search[=STRING]\tsearch for STRING");
    puts("  -a, --add[=LINE]\tadd LINE to the database");
    puts("  -d, --delete[=STRING]\tdelete line containing STRING");
    puts("  -c, --change[=STRING]\tchange the specified STRING");
}
void print_version()
{
  puts("bpdb, version 2021.02.19-1051");
  puts("Copyright (C) 2019-2021 Brian Haslett");
  puts("License GPLv2\n");
  puts("This program is free software; you can redistribute it and/or");
  puts("modify it under the terms of the GNU General Public License version 2");
  puts("as published by the Free Software Foundation.\n");

  puts("This program is distributed in the hope that it will be useful,");
  puts("but WITHOUT ANY WARRANTY; without even the implied warranty of");
  puts("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
  puts("GNU General Public License for more details.\n");

  puts("You should have received a copy of the GNU General Public License");
  puts("along with this program; if not, write to the Free Software");
  puts("Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n");
}

