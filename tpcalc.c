/*
 * Copyright (C) 2020 Brian Haslett
 *    knotwurk@gmail.com
 *
 * toilet paper calculator, calculate how long your toilet paper stash will last
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf
#include <stdlib.h>   // for exit
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <stdbool.h>  // bools
#include <errno.h>    // error types

struct tpdata_s {
  unsigned int sheets_per_roll;
  unsigned int sheets_per_wipe;
  unsigned int rolls;
  unsigned int wipes;
  unsigned int visits;
  unsigned int people;
};

static struct option long_options[] = {
  { "people", 1, NULL, 'p' },
  { "sheets-per-roll", 1, NULL, 's' },
  { "sheets-per-wipe", 1, NULL, 'S' },
  { "rolls", 1, NULL, 'r' },
  { "wipes", 1, NULL, 'w' },
  { "visits", 1, NULL, 'v' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

void calc_supply(struct tpdata_s*);
void print_help(void);

int main(int argc, char *argv[])
{
  bool has_spr=false, has_spw=false, has_rolls=false;
  bool has_wipes=false, has_visits=false, err=false;
  int opt;
  struct tpdata_s tpdata = { .people = 1 };

  if (argc < 2) {
    print_help();
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "p:s:S:r:w:v:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'p':
        sscanf(optarg, "%u", &tpdata.people);
        break;
      case 's':
        sscanf(optarg, "%u", &tpdata.sheets_per_roll);
        has_spr=true;
        break;
      case 'S':
        sscanf(optarg, "%u", &tpdata.sheets_per_wipe);
        has_spw=true;
        break;
      case 'r':
        sscanf(optarg, "%u", &tpdata.rolls);
        has_rolls=true;
        break;
      case 'w':
        sscanf(optarg, "%u", &tpdata.wipes);
        has_wipes=true;
        break;
      case 'v':
        sscanf(optarg, "%u", &tpdata.visits);
        has_visits=true;
        break;
      case 'h':
        print_help();
        return 0;
    }
  }

  if (!tpdata.sheets_per_roll) {
    fprintf(stderr, "Error, missing sheets_per_roll option\n");
    err=true;
  }
  if (!tpdata.sheets_per_wipe) {
    fprintf(stderr, "Error, missing sheets_per_wipe option\n");
    err=true;
  }
  if (!tpdata.rolls) {
    fprintf(stderr, "Error, missing rolls option\n");
    err=true;
  }
  if (!tpdata.wipes) {
    fprintf(stderr, "Error, missing wipes option\n");
    err=true;
  }
  if (!tpdata.visits) {
    fprintf(stderr, "Error, missing visits option\n");
    err=true;
  }

  if (err == true)
    return -EINVAL;

  calc_supply(&tpdata);
  return 0;
}

void calc_supply(struct tpdata_s *data)
{
  unsigned int total_sheets=data->sheets_per_roll * data->rolls;
  unsigned int total_used=data->sheets_per_wipe * data->wipes * data->visits * data->people;
  unsigned int days=total_sheets/total_used;
  printf("You have %u days worth of toilet paper, roughly a %u month supply\n", days, days/30);
}

void print_help(void)
{
  puts("tpcalc");
  puts("Toilet paper calculator, to calculate how long your toilet paper supply will last\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -r, --rolls[=ROLLS]\t\t\tNumber of rolls");
  puts("  -s, --sheets-per-roll[=SHEETS]\tNumber of sheets per roll");
  puts("  -S, --sheets-per-wipe[=SHEETS]\tNumber of sheets per wipe");
  puts("  -w, --wipes[=WIPES]\t\t\tNumber of wipes per visit");
  puts("  -v, --visits[=VISITS]\t\t\tNumber of bathroom visits per day");
  puts("  -p, --people[=PEOPLE]\t\t\tNumber of people using toilet paper (default: 1)");
  puts("  -h, --help\t\t\t\tYou are here");
}
