/*
 * Copyright (C) 2019-2021 Brian Haslett
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _BPDB
#define _BPDB

#include <getopt.h>     // for getopt, getopt_long, struct option
#include <sys/stat.h>    // for stat

/*
 * the first link in a line stores the number of fields for that line
 */

enum fileops {
  NONE,
  ENCRYPT,
  DECRYPT,
  PRINT,
  SEARCH,
  ADD,
  CHANGE,
  DELETE,
  QUIT
};

struct ll_node {
  bool delete;
  int num_fields;
  size_t data_len;
  char *data;
  struct ll_node *prev;
  struct ll_node *next;
};

struct ll_common {
  int num_lines;
  struct multipass *multi;
  struct ll_node *node;
  struct ll_node *line_head;
  struct ll_node *head;
  struct ll_node *tail;
};

static struct option long_options[] = {
  { "delete", 1, NULL, 'd' },
  { "add", 1, NULL, 'a' },
  { "search", 1, NULL, 's' },
  { "change", 1, NULL, 'c' },
  { "print", 0, NULL, 'p' },
  { "file", 1, NULL, 'f' },
  { "help", 0, NULL, 'h' },
  { "encrypted", 0, NULL, 'e' },
  { "encrypt", 0, NULL, 'E' },
  { "version", 0, NULL, 'v' },
  { NULL, 0, NULL, 0 }
};

struct input_args {
  bool use_menu;
  bool update_file;
  bool encrypted_file;
  size_t input_len;
  enum fileops fop;
  char *user_input;
  struct stat sb;
};

void encrypt_file(struct multipass*, struct input_args*);
void decrypt_file(struct multipass*, struct input_args*);
int decrypt_to_mem(struct multipass*, struct ll_common*);
int decrypt_to_buf(struct multipass*, char**);

void parse_args(struct input_args*, int, char*[]);
enum fileops display_menu(struct input_args*);
size_t parse_input_menu(struct input_args*);

size_t get_file_len(struct input_args*);

size_t parse_newline(char**);
void add_encrypted(struct input_args*, struct multipass*);
void add_plain(struct input_args*);
void change_encrypted(struct input_args*, struct ll_common*, struct multipass*);
void change_plain(struct input_args*, struct ll_common*);
void delete_encrypted(struct input_args*, struct ll_common*, struct multipass*);
void delete_plain(struct input_args*, struct ll_common*);
void search_encrypted(struct input_args*, struct ll_common*, struct multipass*);
void search_plain(struct input_args*, struct ll_common*);
void print_encrypted(struct ll_common*, struct multipass*);
void print_plain(struct ll_common*);

void init_data_from_file(struct ll_common*, FILE*);
size_t get_line_from_mem(char**, char*, int);
void init_data_from_mem(struct ll_common*, struct multipass*);
void search_data(struct ll_common*, struct input_args*);

void print_data(struct ll_common*);
size_t parse_input(char**);

void parse_line(struct ll_common*, bool, bool);
void delete_data(struct ll_common*, struct input_args*);
void change_data(struct ll_common*, struct input_args*);
void replace_field(struct ll_common*, struct input_args*);
void write_plain(struct ll_common*);
void write_encrypted(struct ll_common*, struct multipass*);
size_t get_line_len(struct ll_common*);
size_t get_db_len(struct ll_common*);

void free_data(struct ll_common*);
void print_help();
void print_version();

#define FOP_CHECK(x) \
  ({ \
    if (x != NONE) { \
      fprintf(stderr, "Error, only one file operation allowed\n"); \
      exit(EXIT_FAILURE); \
    } \
  })

#define CHUNK 8

#define MMAP_CHECK(x) \
  ({ \
    if (x == MAP_FAILED) { \
      fprintf(stderr, "Error allocating memory: %s\n", strerror(errno)); \
      exit(EXIT_FAILURE); \
    } \
  })
#define MMAP_CHECK2(x, y) \
  ({ \
    if (x == MAP_FAILED || y == MAP_FAILED) { \
      fprintf(stderr, "Error allocating memory: %s\n", strerror(errno)); \
      exit(EXIT_FAILURE); \
    } \
  })
#define MMAP_CHECK3(x, y, z) \
  ({ \
    if (x == MAP_FAILED || y == MAP_FAILED || z == MAP_FAILED) { \
      fprintf(stderr, "Error allocating memory: %s\n", strerror(errno)); \
      exit(EXIT_FAILURE); \
    } \
  })

#define MREMAP_CHECK(old_buf, old_size, new_buf) \
  ({ \
    if (new_buf == MAP_FAILED) { \
      fprintf(stderr, "Error allocating memory: %s\n", strerror(errno)); \
      munmap(old_buf, old_size); \
      exit(EXIT_FAILURE); \
    } \
  })

#define MALLOC_CHECK(x) \
  ({ \
    if (x == NULL) { \
      fprintf(stderr, "Error allocating memory: %s\n", strerror(errno)); \
      exit(EXIT_FAILURE); \
    } \
  })
#define MALLOC_CHECK2(x, y) \
  ({ \
    if (x == NULL || y == NULL) { \
      fprintf(stderr, "Error allocating memory: %s\n", strerror(errno)); \
      exit(EXIT_FAILURE); \
    } \
  })

#define FP_CHECK(x) \
  ({ \
    if (x == NULL) { \
      fprintf(stderr, "Error opening file \"%s\": %s\n", filename, strerror(errno)); \
      exit(EXIT_FAILURE); \
    } \
  })

#endif // _BPDB
