/*
 * Copyright (C) 2017-2020 Brian Haslett
 *
 * encrypt or decrypt messages using various encryption algorithms
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* some bash tests
 * for i in {a..z}; do for j in {a..z}; do ./sneakers -a vigenere -b -k "$j" -d -m "$i"; done; echo ""; done
 * for i in {a..z}; do for j in {a..z}; do ./sneakers -a vigenere -b -k "$j" -e -m "$i"; done; echo ""; done
 *
 * for i in {A..Z}; do for j in {A..Z}; do ./sneakers -a vigenere -b -k "$j" -d -m "$i"; done; echo ""; done
 * for i in {A..Z}; do for j in {A..Z}; do ./sneakers -a vigenere -b -k "$j" -e -m "$i"; done; echo ""; done
 *
 * ./sneakers -bea multipass -k duck -r 16 -m "a long string of text" | ./sneakers -da multipass -k duck -r 16 -m -
 */

#include <stdio.h>    // for printf
#include <stdlib.h>   // for malloc
#include <string.h>   // for strlen, strcpy, memcpy
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <ctype.h>    // for isalpha
#include <errno.h>

#include <math.h> // for sqrt
#include "sneakers.h"

static struct option long_options[] = {
  { "decrypt", 0, NULL, 'd' },
  { "encrypt", 0, NULL, 'e' },
  { "algorithm", 1, NULL, 'a' },
  { "message", 1, NULL, 'm' },
  { "filename", 1, NULL, 'f' },
  { "key", 1,  NULL, 'k' },   // vigenere
  { "rounds", 1, NULL, 'r' },  // multipass,multipass2
  { "shift", 1, NULL, 's' },  // caesar
  { "substitute", 1, NULL, 'S' }, // substitution
  { "batchmode", 0, NULL, 'b' },  // vigenere
  { "version", 0, NULL, 'V' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

int main(int argc, char *argv[])
{
  int opt, msg_count = 0, rv = 0;
  char *alg = NULL, *msg_buf = NULL, *filename = NULL;
  FILE *fp = NULL;
  struct bools_s b = {};
  struct shared_s shared = {};
  struct sub_s su = { .shared = &shared };
  struct spiral_s sp = { .shared = &shared };
  struct caes_s ca = { .shared = &shared };
  struct vig_s vig = { .shared = &shared };
  struct rail_s rail = { .shared = &shared };
  struct multi_s multi = {
    .shared = &shared,
    .vig = &vig,
    .rail = &rail,
    .sp = &sp
  };

  if (argc < 2) {
    print_help();
    return -EINVAL;
  }

  while ((opt = getopt_long(argc, argv, "dea:m:f:k:r:s:S:bVh",
                long_options, NULL)) != -1) {
    switch (opt) {
      case 'a':
        alg = malloc(strlen(optarg)+1);
        if (!alg) {
          fprintf(stderr, "Error allocating memory in main (alg)\n");
          rv = -ENOMEM;
          b.has_failed=true;
          break;
        }

        strcpy(alg, optarg);
        if (strcmp(alg, "vigenere") &&
          strcmp(alg, "caesar-shift") &&
          strcmp(alg, "substitution") &&
          strcmp(alg, "multipass") &&
          strcmp(alg, "multipass2") &&
          strcmp(alg, "multipass3")) {
          fprintf(stderr, "Error, invalid algorithm chosen\n");
          rv = -EINVAL;
          b.has_failed=true;
          break;
        }
        b.has_alg = true;
        break;
      case 'd':
        b.do_decrypt = true;
        break;
      case 'e':
        b.do_encrypt = true;
        break;
      case 's':
        b.has_shift = true;
        sscanf(optarg, "%u", &ca.shift);

        break;
      case 'f':
        shared.file_mode = true;
        filename = malloc(strlen(optarg)+1); 
        if (filename == NULL) {
          fprintf(stderr, "Error allocating memory in main (filename)\n");
          rv = -ENOMEM;
          b.has_failed=true;
          break;
        }
        strcpy(filename, optarg);

        break;
      case 'm':
        rv = parse_message(optarg, &shared);
        if (rv == 0) {
          b.has_message = true;
        } else {
          b.has_failed = true;
        }

        break;
      case 'k':
        vig.key_len = strlen(optarg);
        vig.key = malloc(vig.key_len+1);
        if (!vig.key) {
          fprintf(stderr, "Error allocating memory in main (key)\n");
          rv = -ENOMEM;
          b.has_failed = true;
        }

        strcpy(vig.key, optarg);

        b.has_key = true;

        break;
      case 'S':
        memcpy(su.sub_alphabet, optarg, 26);
        b.has_sub = true;
        break;
      case 'r':
        b.has_rounds = true;
        sscanf(optarg, "%u", &multi.rounds);
        break;
      case 'b':
        shared.batch_mode = true;
        break;
      case 'V':
        print_version();
        return 0;
      case 'h':
        print_help();
        return 0;
      default:
        print_help();
        return 0;
    }
  }

  rv = error_check(&b, &shared);

  if (b.has_alg == false) {
    fprintf(stderr, "Error, no algorithm was set\n");
    rv = -EINVAL;
    b.has_failed=true;
  } else {
    if (!strcmp(alg, "vigenere")
        && b.has_key == false) {

      fprintf(stderr, "Error, the key isn't set\n");
      rv = -EINVAL;
      b.has_failed=true;

    } else if (!strcmp(alg, "caesar-shift")
               && b.has_shift == false) {

      fprintf(stderr, "Error, the shift vlaue isn't set\n");
      rv = -EINVAL;
      b.has_failed=true;

    } else if (!strcmp(alg, "substitution")
               && b.has_sub == false) {

      fprintf(stderr, "Error, the substitution alphabet isn't set\n");
      rv = -EINVAL;
      b.has_failed=true;

    } else if (!strcmp(alg, "multipass") || !strcmp(alg, "multipass2") || !strcmp(alg, "multipass3")) {
      if (b.has_key == false) {
        fprintf(stderr, "Error, the key isn't set\n");
        rv = -EINVAL;
        b.has_failed=true;
      }
      if (b.has_rounds == false) {
        fprintf(stderr, "Error, number of rounds isn't set\n");
        rv = -EINVAL;
        b.has_failed=true;
      }
    }
  }

  if (b.has_failed == false) {
    if (shared.file_mode == true) {
      puts("This will encrypt the file, overwriting whatever is currently there");
      rv = run_confirmation();
      if (rv == -1)
        goto done;

      fp = fopen(filename, "r+");
      if (!fp) {
        fprintf(stderr, "Error, file not found.\n");
        goto done;
      }
      fseek(fp, 0L, SEEK_END);
      shared.msg_len = ftell(fp);
      fseek(fp, 0L, SEEK_SET);

      shared.msg_str = malloc(shared.msg_len+1);
      shared.cipher_str = malloc(shared.msg_len+1);
      fread(shared.msg_str, shared.msg_len, 1, fp);
    }

    if (b.do_encrypt == true) {
      if (!strcmp(alg, "vigenere")) {
        encrypt_vigenere(vig.shared->cipher_str, vig.shared->msg_str, vig.shared->msg_len, vig.key, vig.key_len);
      } else if (!strcmp(alg, "caesar-shift")) {
        encrypt_caesar(&ca);
      } else if (!strcmp(alg, "substitution")) {
        run_substitution(&su);
      } else if (!strcmp(alg, "multipass")) {
        encrypt_multi(&multi);
      } else if (!strcmp(alg, "multipass2")) {
        encrypt_multi2(&multi);
      } else if (!strcmp(alg, "multipass3")) {
        encrypt_multi3(&multi);
      }
      print_output(&shared);
    } else if (b.do_decrypt == true) {
      if (!strcmp(alg, "vigenere")) {
        decrypt_vigenere(vig.shared->cipher_str, vig.shared->msg_str, vig.shared->msg_len, vig.key, vig.key_len);
      } else if (!strcmp(alg, "caesar-shift")) {
        decrypt_caesar(&ca);
      } else if (!strcmp(alg, "substitution")) {
        run_substitution(&su);
      } else if (!strcmp(alg, "multipass")) {
        decrypt_multi(&multi);
      } else if (!strcmp(alg, "multipass2")) {
        decrypt_multi2(&multi);
      } else if (!strcmp(alg, "multipass3")) {
        decrypt_multi3(&multi);
      }
      print_output(&shared);
    }
  } else {
    print_help();
    rv = -EINVAL;
    goto done;
  }

  if (shared.file_mode == true) {
    fseek(fp, 0L, SEEK_SET);
    fwrite(shared.cipher_str, shared.msg_len, 1, fp);
    fclose(fp);
    free(filename);
  }

done:
  free(shared.msg_str);
  free(shared.cipher_str);
  free(vig.key);
  free(alg);
  return rv;
}

int error_check(struct bools_s *b, struct shared_s *shared)
{
  int rv = 0;
  if (b->do_encrypt == false && b->do_decrypt == false) {
    fprintf(stderr, "Error must choose encrypt or decrypt\n");
    rv = -EINVAL;
    b->has_failed=true;
  }
  if (b->do_encrypt == true && b->do_decrypt == true) {
    fprintf(stderr, "Error, choose encrypt or decrypt, not both.\n");
    rv = -EINVAL;
    b->has_failed=true;
  }
  if (shared->file_mode == true && b->has_message == true) {
    fprintf(stderr, "Error, set message or filename, not both.\n");
    rv = -EINVAL;
    b->has_failed=true;
  }
  if (b->has_message == false && shared->file_mode == false) {
    fprintf(stderr, "Error, no message or filename was set\n");
    rv = -EINVAL;
    b->has_failed=true;
  }

  return rv;
}

/*
 * run the confirmation dialog
 *
 * returns -1 if user wishes to cancel
 */
int run_confirmation()
{
  char c;
try_again:
  printf("Do you wish to proceed (y/n)? ");
  c=getchar();
  if (c == 'y') {
    return 0;
  } else if (c == 'n') {
    puts("Exiting..");
    return -1;
  } else {
    puts("Input not understood");
    goto try_again;
  }
}

/*
 * parses user input
 *
 * returns 0 upon success, error upon failure
 */
int parse_message(char *optarg, struct shared_s *shared)
{
  int msg_count = 0, c;


  if (!strcmp(optarg,"-")) {  // read from stdin
    while ((c = getchar()) != EOF) {
      msg_count++;

      shared->msg_str = realloc(shared->msg_str,msg_count+1);
      if (!shared->msg_str) {
        fprintf(stderr, "Error allocating memory in main (msg_str/stdin)\n");
        return -ENOMEM;
      }

      shared->msg_str[msg_count-1] = c;
    }
    shared->msg_str[msg_count] = '\0';
    shared->msg_len=msg_count;

    shared->cipher_str = malloc(shared->msg_len+1);
    if (!shared->cipher_str) {
      fprintf(stderr, "Error allocating memory in main (cipher_str/stdin)\n");
      return -ENOMEM;
    }

  } else {    // read from commandline options
    shared->msg_len=strlen(optarg);

    shared->msg_str = malloc(shared->msg_len+1);
    shared->msg_str[shared->msg_len]='\0';
    if (!shared->msg_str) {
      fprintf(stderr, "Error allocating memory in main (msg_str)\n");
      return -ENOMEM;
    }

    memcpy(shared->msg_str, optarg, shared->msg_len);

    shared->cipher_str = malloc(shared->msg_len+1);
    if (!shared->cipher_str) {
      fprintf(stderr, "Error allocating memory in main (cipher_str)\n");
      return -ENOMEM;
    }
  }
  return 0;
}

void print_output(struct shared_s *shared)
{
  if (shared->file_mode == false) {
    if (shared->batch_mode == false) {
      printf("\"%s\"\n", shared->cipher_str);
    } else {
      printf("%s", shared->cipher_str);
    }
  }
}

/*
 * vigenere++, a polyalphanumeric substitution cipher
 *
 * input:
 * msg_str - message to encrypt
 * msg_len - length of message to encrypt
 * key - key/passphrase
 * key_len - length of key/passphrase
 *
 * output:
 * cipher_str - cipher text output
 */
void encrypt_vigenere(char *cipher_str, char *msg_str, size_t msg_len, char *key, size_t key_len)
{
  char key_char, msg_char, result;
  int type, key_loop_count, shift, cipher_str_count = 0;

  // shift msg_str according to key, repeatedly
  for (int msg_loop_count=0, key_loop_count=0;
     msg_loop_count < msg_len;
     msg_loop_count++, key_loop_count++) {

    if (key_len == key_loop_count)
      key_loop_count = 0;

    // current letters we are dealing with
    msg_char = msg_str[msg_loop_count];
    key_char = key[key_loop_count];

    if (isdigit(msg_char)) {
      type=DIGIT;
    } else if (isalpha(msg_char)) {
      type=ALPHA;
    } else {
      type=UNDEF;
    }
    
    // type refers to the msg_str characters
    switch (type) {
      case UNDEF:
        key_loop_count--;   // key doesn't increment for these
        cipher_str[cipher_str_count] = msg_char;
        cipher_str_count++;
        break;;
      case DIGIT:
        //  calculate shift, based on key case 
        if (isupper(key_char)) shift = key_char-'A';
        else if (islower(key_char)) shift = key_char-'a';
        else if (isdigit(key_char)) shift = key_char-'0';
        else if (ispunct(key_char)) shift = key_char-'!';

        // handle shift wraparound
        while (shift > 10) shift -= 10;
        while (msg_char+shift > '9') msg_char-=10;

        result = msg_char+shift;
        cipher_str[cipher_str_count] = result;
        cipher_str_count++;

        break;;
      case ALPHA:
        if (isupper(msg_char)) {
           //  calculate shift, based on key case 
          if (isupper(key_char)) shift = key_char-'A';
          else if (islower(key_char)) shift = key_char-'a';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          while ((msg_char+shift) > 'Z') msg_char-=26;

          result = msg_char+shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;

        } else if (islower(msg_char)) {
          //  calculate shift, based on key case 
          if (islower(key_char)) shift = key_char-'a';
          else if (isupper(key_char)) shift = key_char-'A';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          while ((msg_char+shift) > 'z') msg_char-=26;

          result = msg_char+shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;

        }
        break;;
    }
  }
}
void decrypt_vigenere(char *cipher_str, char *msg_str, size_t msg_len, char *key, size_t key_len)
{
  char key_char, msg_char, result;
  int type, key_loop_count, shift, cipher_str_count = 0;

  // shift msg_str according to key, repeatedly
  for (int msg_loop_count=0, key_loop_count=0;
     msg_loop_count < msg_len;
     msg_loop_count++, key_loop_count++) {

    if (key_len == key_loop_count)
      key_loop_count = 0;

    // current letters we are dealing with
    msg_char = msg_str[msg_loop_count];
    key_char = key[key_loop_count];

    if (isdigit(msg_char)) {
      type=DIGIT;
    } else if (isalpha(msg_char)) {
      type=ALPHA;
    } else {
      type=UNDEF;
    }

    switch (type) {
      case UNDEF:
        key_loop_count--;   // key doesn't increment for these
        cipher_str[cipher_str_count] = msg_char;
        cipher_str_count++;
        break;;
      case DIGIT:
        if (isupper(key_char)) shift = key_char-'A';
        else if (islower(key_char)) shift = key_char-'a';
        else if (isdigit(key_char)) shift = key_char-'0';
        else if (ispunct(key_char)) shift = key_char-'!';

        // handle shift wraparound
        while (shift > 10) shift -= 10;
        if (msg_char-shift < '0') msg_char+=10;

        result = msg_char-shift;
        cipher_str[cipher_str_count] = result;
        cipher_str_count++;
        break;;
      case ALPHA:
        if (isupper(msg_char)) {
          //  calculate shift, based on key case 
          if (isupper(key_char)) shift = key_char-'A';
          else if (islower(key_char)) shift = key_char-'a';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';
    
          // handle shift wraparound
          while (shift > 26) shift -= 26;
          if ((msg_char-shift) < 'A') msg_char+=26;

          result = msg_char-shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;
        } else if (islower(msg_char)) {
          //  calculate shift, based on key case 
          if (isupper(key_char)) shift = key_char-'A';
          else if (islower(key_char)) shift = key_char-'a';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          if ((msg_char-shift) < 'a') msg_char+=26;

          result = msg_char-shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;
        }
        break;;
    }
  }
}

/*
 * caesar, a substitution cipher
 *
 * requires a shift value
 */
void encrypt_caesar(struct caes_s *ca)
{
  char current_char;
  int shift, cipher_str_count = 0;
  size_t msg_len = ca->shared->msg_len;

  char result;

  for (int i=0; i<msg_len; i++) {
    shift=ca->shift;
    current_char = ca->shared->msg_str[i];

    if (ispunct(current_char) || isspace(current_char)) {
      ca->shared->cipher_str[cipher_str_count] = current_char;
      cipher_str_count++;

    } else if (isdigit(current_char)) {
      while (shift >= 10) shift-=10;
      while ((current_char+shift) > '9' ) current_char-=10;

      result = current_char+shift;
      ca->shared->cipher_str[cipher_str_count] = result;
      cipher_str_count++;
    } else if (isalpha(current_char) && islower(current_char)) {
      while (shift >= 26) shift-=26;
      while ((current_char+shift) > 'z') current_char-=26;

      result = current_char+shift;
      ca->shared->cipher_str[cipher_str_count] = result;
      cipher_str_count++;
    } else if (isalpha(current_char) && isupper(current_char)) {
      while (shift >= 26) shift-=26;
      while ((current_char+shift) > 'Z') current_char-=26;

      result = current_char+shift;
      ca->shared->cipher_str[cipher_str_count] = result;
      cipher_str_count++;
    }
  }
}
void decrypt_caesar(struct caes_s *ca)
{
  char current_char;
  unsigned int i, shift, msg_len = ca->shared->msg_len;
  int cipher_str_count = 0;
  char result;

  for (i=0; i<msg_len; i++) {
    shift=ca->shift;
    current_char = ca->shared->msg_str[i];

    if (ispunct(current_char) || isspace(current_char)) {
      ca->shared->cipher_str[cipher_str_count] = current_char;
      cipher_str_count++;

    } else if (isdigit(current_char)) {
      while (shift >= 10) shift-=10;
      while ((current_char-shift) < '0' ) current_char+=10;

      result = current_char-shift;
      ca->shared->cipher_str[cipher_str_count] = result;
      cipher_str_count++;
    } else if (isalpha(current_char) && islower(current_char)) {
      while (shift >= 26) shift-=26;
      while ((current_char-shift) < 'a') current_char+=26;

      result = current_char-shift;
      ca->shared->cipher_str[cipher_str_count] = result;
      cipher_str_count++;
    } else if (isalpha(current_char) && isupper(current_char)) {
      while (shift >= 26) shift-=26;
      while ((current_char-shift) < 'A') current_char+=26;

      result = current_char-shift;
      ca->shared->cipher_str[cipher_str_count] = result;
      cipher_str_count++;
    }
  }
}

/*
 * a simple substitution cipher, kept around for historical purposes mostly
 *
 * requires a 26-character substitution alphabet
 */
void run_substitution(struct sub_s *su)
{
  char current_char;
  unsigned int msg_len = su->shared->msg_len;
  int cipher_str_count = 0;
  const char alpha_lowercase[] = "abcdefghijklmnopqrstuvwxyz";
  const char alpha_uppercase[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char alpha_buf[27];

  memcpy(alpha_buf, su->sub_alphabet, 26);
  for (int i=0; i<msg_len; i++) {
    current_char=su->shared->msg_str[i];

    if (ispunct(current_char) || isspace(current_char) || isdigit(current_char)) {
      su->shared->cipher_str[cipher_str_count] = current_char;
      cipher_str_count++;
    } else if (isalpha(current_char)) {
      for (int j=0; j<26; j++) {
        if (islower(current_char)) {
          if (alpha_lowercase[j] == current_char) {
            if (isupper(alpha_buf[j])) {
              su->shared->cipher_str[cipher_str_count] = tolower(alpha_buf[j]);
            } else {
              su->shared->cipher_str[cipher_str_count] = alpha_buf[j];
            }
            cipher_str_count++;
          }
        } else if (isupper(current_char)) {
          if (alpha_uppercase[j] == current_char) {
            if (islower(alpha_buf[j])) {
              su->shared->cipher_str[cipher_str_count] = toupper(alpha_buf[j]);
            } else {
              su->shared->cipher_str[cipher_str_count] = alpha_buf[j];
            }
            cipher_str_count++;
          }
        }
      }
    }
  }

  if (su->shared->file_mode == false) {
    if (su->shared->batch_mode == false) {
      printf("\"%s\"\n", su->shared->cipher_str);
    } else {
      printf("%s", su->shared->cipher_str);
    }
  }
}

/*
 * shared: shared struct containing msg_len length and msg_str string input
 * y_len: column size for 2d array output
 * x_len: row size for 2d array output
 * output: 2d array to send output
 */
void string_to_array(size_t y_len, size_t x_len,
    struct shared_s *shared, char output[][x_len+1])
{
  for (int y=0, len_cnt=0; y < y_len && len_cnt < shared->msg_len; y++)
    for (int x=0; x < x_len && len_cnt < shared->msg_len; x++, len_cnt++)
      output[y][x] = shared->msg_str[len_cnt];
}


/*
 * input: 2d array to read input
 * y_len: column size for 2d array input
 * x_len: row size for 2d array input
 * shared: shared struct containing msg_len size and cipher_str string output
 */
void array_to_string(size_t y_len, size_t x_len,
    struct shared_s *shared, char input[][x_len+1])
{
  for (int y=0, len_cnt=0; y < y_len; y++)
    for (int x=0; x < x_len && len_cnt < shared->msg_len; x++, len_cnt++)
      shared->cipher_str[len_cnt] = input[y][x];
}

/*
 * y_len: column size
 * x_len: row size
 * rem: remainders in the last row
 * output: output string
 */
void write_null_bytes(size_t y_len, size_t x_len,
    int rem, char output[][x_len+1])
{
  for (int i=0; i<y_len; i++) {
    output[i][x_len]='\0';
  }
  if (rem != 0) {
    for (int i=x_len-rem; i <= x_len; i++) {
      output[y_len-1][i]='\0';
    }
  }
}

/*
 * adjust y_len as necessary
 *
 * y_len: column size
 * x_len: row size
 * msg_len: total size of message
 *
 * yB needs to be reinitialized after this is called
 * returns number of remainders in the last row
 */
int calc_remainders(size_t msg_len, size_t x_len, size_t *y_len)
{
  int rem = 0;

  while ((x_len * (*y_len)) < msg_len) {
    (*y_len)++;
  } 
  if (msg_len - (x_len * (*y_len)) == 0)
    rem=0;
  else
    rem = (x_len * (*y_len)) - msg_len;
  return rem;
}

/*
 * the syntax for these spiral direction functions is pretty much the same
 * 
 * sp:  main spiral struct containing most of the variables needed
 * x_len:  size of rows in I/O text
 * input_text:  the input text
 * output_text:  the output text
 * simulate:  simulation mode; used in first half of the decryption process
 *
 * if running in simulation mode, output_text, input_text, and x_len are not used
 */
void spiral_left(struct spiral_s *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->x >= sp->xL && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x]; 
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->x != sp->xL)
      sp->x--;
    else
      break;
  }
}
void spiral_up(struct spiral_s *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->y >= sp->yT && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x]; 
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->y != sp->yT)
      sp->y--;
    else
      break;
  }
}
void spiral_right(struct spiral_s *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->x <= sp->xR && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x]; 
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->x != sp->xR)
      sp->x++;
    else
      break;
  }
}
void spiral_down(struct spiral_s *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->y <= sp->yB && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x]; 
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->y != sp->yB)
      sp->y++;
    else
      break;
  }
}
void init_spiral(struct spiral_s *sp, size_t x_len, size_t y_len)
{
  sp->len_cnt = 0;
  sp->xL = 0;
  sp->yT = 0;
  sp->x_out = 0;
  sp->y_out = 0;
  sp->xR = x_len - 1;
  sp->yB = y_len - 1;
}
/*
 * spiral, a transposition cipher
 *
 * it's mostly just used in multipass2 and multipass3 to help reduce collisions,
 * since it doesn't really take a key or anything
 *
 * x's left and right (xL/xR) change along with
 * y's top and bottom (yT/yB) as it spirals inward (encrypt) or outward (decrypt)
 */
void encrypt_spiral(struct spiral_s *sp)
{
  int rem = 0;
  size_t x_len = (int) sqrt(sp->shared->msg_len), y_len = x_len;

  init_spiral(sp, x_len, y_len);

  /* sqrt returns rounded figure, y_len may need adjusting */
  if ((sp->shared->msg_len - (x_len * y_len)) != 0) {
    rem = calc_remainders(sp->shared->msg_len, x_len, &y_len);
    sp->yB = y_len-1;
  }
  char input_text[y_len][x_len+1], output_text[y_len][x_len+1];

  write_null_bytes(y_len, x_len, rem, input_text);
  write_null_bytes(y_len, x_len, rem, output_text);
  string_to_array(y_len, x_len, sp->shared, input_text);

  if (rem != 0) {
    sp->x=x_len-rem-1;
    sp->y=y_len-1;
  } else {
    sp->x=x_len-1;
    sp->y=y_len-1;
  }

  /* the main loop */
  while (sp->len_cnt != sp->shared->msg_len) {
    spiral_left(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->yB--;
    sp->y--;

    spiral_up(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->xL++;
    sp->x++;

    spiral_right(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->yT++;
    sp->y++;

    spiral_down(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->xR--;
    sp->x--;
  }
done:

  array_to_string(y_len, x_len, sp->shared, output_text);
}
void decrypt_spiral(struct spiral_s *sp)
{
  int rem = 0;
  size_t x_len = (int) sqrt(sp->shared->msg_len), y_len = x_len;
  init_spiral(sp, x_len, y_len);

  /* sqrt returns rounded figure, y_len may need adjusting */
  if ((sp->shared->msg_len - (x_len * y_len)) != 0) {
    rem = calc_remainders(sp->shared->msg_len, x_len, &y_len);
    sp->yB = y_len-1;
  }
  char output_text[y_len][x_len+1];
  write_null_bytes(y_len, x_len, rem, output_text);

  if (rem != 0) {
    sp->x=x_len-rem-1;
    sp->y=y_len-1;
  } else {
    sp->x=x_len-1;
    sp->y=y_len-1;
  }

  /* simulate the main loop */
  while (sp->len_cnt != sp->shared->msg_len) {
    spiral_left(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto right;
    sp->yB--;
    sp->y--;

    spiral_up(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto down;
    sp->xL++;
    sp->x++;

    spiral_right(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto left;
    sp->yT++;
    sp->y++;

    spiral_down(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto up;
    sp->xR--;
    sp->x--;
  }

  /*
   * spiral outwards from the center
   * while reading the input cipher backwards
   * 
   * simulation gives an entry based on when
   * len_cnt reaches msg_len
   */
  while (sp->len_cnt != 0) {
up:
    sp->yT--;
    while (sp->y >= sp->yT && sp->len_cnt != 0) {
        output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
        sp->len_cnt--;
        if (sp->y != sp->yT)
          sp->y--;
        else
          break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->x--;
left:
    sp->xL--;
    while (sp->x >= sp->xL && sp->len_cnt != 0) {
      output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
      sp->len_cnt--;
      if (sp->x != sp->xL)
        sp->x--;
      else
        break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->y++;
down:
    sp->yB++;
    while (sp->y <= sp->yB && sp->len_cnt != 0) {
      output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
      sp->len_cnt--;
      if (sp->y != sp->yB)
        sp->y++;
      else
        break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->x++;
right:
    sp->xR++;
    while (sp->x <= sp->xR && sp->len_cnt != 0) {
      output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
      sp->len_cnt--;
      if (sp->x != sp->xR)
        sp->x++;
      else
        break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->y--;
  }

done:
  array_to_string(y_len, x_len, sp->shared, output_text);
}

/*
 * rail-fence, a transposition cipher
 *
 * input:
 * msg_str - message to encrypt
 * msg_len - length of message to encrypt
 * fence_height - height of the rail-fence
 *
 * output:
 * cipher_str - cipher text output
 */
void encrypt_rail(char *cipher_str, char *msg_str, size_t msg_len, size_t fence_height)
{
  int rem, rem_count = 0;
  int num, num_no_rem;

  rem = msg_len % fence_height;
  num = msg_len/fence_height + (rem>0 ? 1 : 0);
  if (rem > 0) {
    num_no_rem = num-1;
  }
  char fence[fence_height][num];

  /*
   * store the plaintext in column-major order in a 2d array
   */
  for (int x=0, z=0; x < num; x++) {
    for (int y=0; y < fence_height && z < msg_len; y++, z++) {
      fence[y][x] = msg_str[z];
    }
  }

  /*
   * read back the cipher_str in row-major order
   */
  for (int y=0, z=0; y < fence_height; y++) {
    for (int x=0; x < num && z < msg_len; x++, z++) {
      cipher_str[z] = fence[y][x];
    }
    if (rem > 0 && rem_count != rem) {
      rem_count++;
      if (rem_count == rem) {
        num = num_no_rem;
      }
    }
  }
}
void decrypt_rail(char *cipher_str, char *msg_str, size_t msg_len, size_t fence_height)
{
  int rem, rem_count = 0;
  int num, num_no_rem;

  rem = msg_len % fence_height;
  num = msg_len/fence_height + (rem>0 ? 1 : 0);
  if (rem > 0) {
    num_no_rem = num-1;
  }
  char fence[fence_height][num+1];

  /*
   * store the cipher_str in row-major order in a 2d array
   */
  for (int y=0, z=0; y < fence_height; y++) {
    for (int x=0; x < num && z < msg_len; x++, z++) {
      fence[y][x] = msg_str[z];
    }
    if (rem > 0 && rem_count != rem) {
      rem_count++;
      if (rem_count == rem) {
        num = num_no_rem;
      }
    }
  }

  /*
   * read back the plaintext in column-major order
   */
  if (rem > 0) {
    num++;
  }
  for (int x=0, z=0; x < num; x++) {
    for (int y=0; y < fence_height && z < msg_len; y++) {
      cipher_str[z] = fence[y][x];
      z++;
    }
  }
}

/*
 * multipass, a combination of substitution and transposition ciphers
 *
 * set the key and number of rounds
 */
void encrypt_multi(struct multi_s *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  multi->shared->multi_mode = true;

  for (int i=0; i<rounds; i++) {
    encrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->vig->key, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
  }
}
void decrypt_multi(struct multi_s *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  multi->shared->multi_mode = true;

  for (int i=0; i<rounds; i++) {
    decrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->vig->key, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
  }
}

/*
 * multipass2, a combination of substitution and transposition ciphers
 * much more robust than multipass with regard to collisions
 *
 * set the key and number of rounds
 */
void encrypt_multi2(struct multi_s *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  multi->shared->multi_mode = true;

  for (int i=0; i<rounds; i++) {
    encrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->vig->key, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_spiral(multi->sp);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
  }
}
void decrypt_multi2(struct multi_s *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  multi->shared->multi_mode = true;

  for (int i=0; i<rounds; i++) {
    decrypt_spiral(multi->sp);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->vig->key, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
  }
}

/*
 * multipass3, a combination of substitution and transposition ciphers
 * the key generates its own cipher which in turn is used to encrypt the message
 *
 * set the key and number of rounds
 */
void encrypt_multi3(struct multi_s *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  multi->shared->multi_mode = true;
  char *key_cipher = malloc(multi->vig->key_len+1);
  char *key_buf = malloc(multi->vig->key_len+1);
  memcpy(key_cipher, multi->vig->key, multi->vig->key_len);

  for (int i=0; i<rounds; i++) {
    /* encrypt the key */
    encrypt_vigenere(key_buf, key_cipher, multi->vig->key_len, multi->vig->key, multi->vig->key_len);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
    encrypt_rail(key_buf, key_cipher, multi->vig->key_len, multi->rail->fence_height/2);
    memcpy(key_cipher, key_buf, multi->vig->key_len);

    /* encrypt the message */
    encrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, key_cipher, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_spiral(multi->sp);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
  }

  free(key_cipher);
  free(key_buf);
}
void decrypt_multi3(struct multi_s *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  multi->shared->multi_mode = true;
  char *key_cipher = malloc(multi->vig->key_len+1);
  char *key_buf = malloc(multi->vig->key_len+1);
  memcpy(key_cipher, multi->vig->key, multi->vig->key_len);

  /* simulate the key encryption, and work backwards */
  for (int i=0; i<rounds; i++) {
    encrypt_vigenere(key_buf, key_cipher, multi->vig->key_len, multi->vig->key, multi->vig->key_len);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
    encrypt_rail(key_buf, key_cipher, multi->vig->key_len, multi->rail->fence_height/2);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
  }
  for (int i=0; i<rounds; i++) {
  /* decrypt the message */
    decrypt_spiral(multi->sp);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, key_cipher, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);

    /* decrypt the key */
    decrypt_rail(key_buf, key_cipher, multi->vig->key_len, multi->rail->fence_height/2);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
    decrypt_vigenere(key_buf, key_cipher, multi->vig->key_len, multi->vig->key, multi->vig->key_len);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
  }

  free(key_cipher);
  free(key_buf);
}
void print_version()
{
  puts("sneakers, version 2020.03.09-1422");
  puts("Copyright (C) 2017-2020 Brian Haslett");
  puts("License GPLv2\n");
  puts("This program is free software; you can redistribute it and/or");
  puts("modify it under the terms of the GNU General Public License version 2");
  puts("as published by the Free Software Foundation.\n");

  puts("This program is distributed in the hope that it will be useful,");
  puts("but WITHOUT ANY WARRANTY; without even the implied warranty of");
  puts("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
  puts("GNU General Public License for more details.\n");

  puts("You should have received a copy of the GNU General Public License");
  puts("along with this program; if not, write to the Free Software");
  puts("Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n");
}
void print_help()
{
  puts("\nsneakers");
  puts("encrypt/decrypt messages\n");
  puts("Usage: [options]");
  puts("Options:");
  puts("  -d, --decrypt\t\t\t"
     "decrypt a message");
  puts("  -e, --encrypt\t\t\t"
     "encrypt a message");
  puts("  -a, --algorithm[=ALG]\t\t"
     "use ALG encryption algorithm; choices are\n"
     "\t\t\t\tvigenere, caesar-shift, substitution, multipass, multipass2, or multipass3");
  puts("  -m, --message[=MESSAGE]\t"
     "message to encrypt/decrypt; use - for stdin.");
  puts("  -f, --filename=[FILE]\t"
     "\tFILE to encrypt/decrypt");
  puts("  -k, --key[=KEY]\t\t"
     "use KEY to encrypt/decrypt message;\n"
     "\t\t\t\tKEY can be alphanumeric or punctuation;\n"
     "\t\t\t\t** required for vigenere, multipass, multipass2, multipass3");
  puts("  -r, --rounds[=ROUNDS]\t\t"
     "number of ROUNDS to use;\n"
     "\t\t\t\t** required for multipass, multipass2, multipass3");
  puts("  -s, --shift[=COUNT]\t\t"
     "amount of SHIFT;\n"
     "\t\t\t\t** required for caesar-shift");
  puts("  -S, --substitute=[=ALPHABET]\t"
     "ALPHABET to substitute;\n"
     "\t\t\t\t** required for substitution;\n"
     "\t\t\t\tNOTE: must be 26 characters long!");
  puts("  -b, --batchmode\t\t"
     "don't add new lines or quotes (for scripts/testing)");
  puts("  -V, --version\t\t\tshow version information");
  puts("  -h, --help\t\t\tyou are here\n");
  puts("either a message or a filename must be supplied for input\n");
}
