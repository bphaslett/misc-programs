/*
 * Copyright (C) 2019-2021 Brian Haslett
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _BPDB_CRYPT
#define _BPDB_CRYPT

struct shared_text {
  size_t msg_len;
  char *msg_str;
  char *cipher_str;
};

struct vigenere {
  size_t key_len;
  char *key;
  struct shared_text *shared;
};

struct spiral {
  int len_cnt;
  int x, y;
  int x_out, y_out;
  int xL, xR, yT, yB;
  struct shared_text *shared;
};

struct railfence {
  size_t fence_height;
  struct shared_text *shared;
};

struct multipass {
  bool set;
  int rounds;
  struct shared_text *shared;
  struct vigenere *vig;
  struct railfence *rail;
  struct spiral *sp;
};

int decrypt_mem(struct multipass*, FILE*);
int encrypt_mem(struct multipass*, char*);
int decrypt_file_multi3(struct multipass*, FILE*);
int encrypt_file_multi3(struct multipass*, FILE*);
int get_crypt_info(struct multipass*);
int set_crypt_info(struct multipass*);
int echo_on(void);
int echo_off(bool*);

#endif // _BPDB_CRYPT
