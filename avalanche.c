/*
 * Copyright (C) 2019 Brian Haslett
 *    knotwurk@gmail.com
 *
 * avalanche v2.0 🛷
 * NOP sled detector
 *
 * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.459.67&rep=rep1&type=pdf
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#define _LARGEFILE64_SOURCE     /* See feature_test_macros(7) */
#include <sys/types.h>  // for lseek, open
#include <unistd.h>     // for lseek
#include <sys/stat.h>   // for open
#include <fcntl.h>      // for open

#include <stdio.h>      // for printf, getline, scanf
#include <stdlib.h>     // for exit
#include <string.h>     // for strstr
#include <ctype.h>      // for isdigit
#include <getopt.h>     // for getopt, getopt_long, struct option
#include <sys/mman.h>   // for mlock
#include <stdbool.h>    // for bools

#include "avalanche.h"

bool sel_verbose=false;

int root_check()
{
  if (geteuid() != 0) {
    fprintf(stderr, "Error, please run as root\n");
		return -1;
  } else {
		return 0;
	}
}

int main(int argc, char *argv[])
{
  bool do_procscan=false, do_memscan=false;
  int opt, pid;

  if (argc < 2) {
    print_help();
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "vp:mh", long_options, NULL)) != -1) {
    switch (opt) {
      case 'v':
        sel_verbose=true;
        break;
      case 'p':
				if (root_check() == -1)
					exit(EXIT_FAILURE);
        pid=atoi(optarg);
        do_procscan=true;
        system("echo 3 > /proc/sys/vm/drop_caches");
        break;
      case 'm':
				if (root_check() == -1)
					exit(EXIT_FAILURE);
        system("echo 3 > /proc/sys/vm/drop_caches");
        do_memscan=true;
        break;
      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
        break;
      default:
        print_help();
        return 0;
    }
  }
  if (do_procscan == true)
    scan_proc(pid);
  if (do_memscan == true)
    scan_mem();
    
  return 0;
}

size_t get_free_bytes()
{
  int i=0, j=0;
  size_t bytes=0, buf_len;
  char *read_buf=NULL, *write_buf = NULL;
  FILE *fp = fopen("/proc/meminfo", "r");
  
  while (getline(&read_buf, &bytes, fp) != -1) {
    if ((strstr(read_buf, "MemAvailable") != NULL)) {
      while (read_buf[i] != '\n') {
        if (isdigit(read_buf[i])) {
          j++;
          write_buf = realloc(write_buf, j);
          write_buf[j-1] = read_buf[i];
        }
        i++;
      }    
    }
  }

  sscanf(write_buf, "%zd", &buf_len);

  /*
   * https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-meminfo
   */
  buf_len *= 1024;

  free(read_buf);
  free(write_buf);
  fclose(fp);
  return buf_len;
}

/*
 * pid: pid to scan
 */
void scan_proc(unsigned int pid)
{
  bool nops_detected, reset_loop=false;
  int mem_fd, count=0, match_start, matched;
  unsigned char one_byte, two_bytes[2], three_bytes[3], four_bytes[4];
  unsigned char five_bytes[5], six_bytes[6], seven_bytes[7], eight_bytes[8];
  unsigned char nine_bytes[9], ten_bytes[10], eleven_bytes[11];
  size_t buf_len, bytes=0, maps_total=0, write_matches=0;
  char *read_buf=NULL;
  unsigned char *write_buf = NULL;
  char maps_filename[12+PID_MAX_LEN], mem_filename[11+PID_MAX_LEN];
  char begin[ADDR_MAX_LEN+1], end[ADDR_MAX_LEN+1];
  void *addr_begin, *addr_end;
  FILE *fp = NULL;

  sprintf(maps_filename, "/proc/%u/maps", pid);
  sprintf(mem_filename, "/proc/%u/mem", pid);
  mem_fd = open(mem_filename, O_RDWR);
  fp = fopen(maps_filename, "r");

  printf("Scanning writable memory in pid %d\n", pid);

  while (getline(&read_buf, &bytes, fp) != -1) {
    if ((strstr(read_buf, " rw") != NULL) && (strstr(read_buf, "[stack") == NULL)) {
      int i, j;
      for (i=0; read_buf[i] != '-'; i++) {
          begin[i] = read_buf[i];
      }
      begin[i++]='\0';
      for (j=0; read_buf[i] != ' '; j++, i++) {
        end[j] = read_buf[i];
      }
      end[j]='\0';
    } else {
      continue;
    }
    sscanf(begin, "%p", &addr_begin);
    sscanf(end, "%p", &addr_end);
    buf_len = addr_end - addr_begin;
    if (sel_verbose == true) {
      printf("Range calculated:  %p-%p %zd bytes\n", addr_begin, addr_end, buf_len);
    }

    write_buf=realloc(write_buf,buf_len);
    lseek64(mem_fd,(unsigned long)addr_begin,SEEK_SET);
    read(mem_fd,write_buf,buf_len);

    mlock(write_buf,buf_len);

    for (int i=0; i < buf_len; i++) {
retry:
      nops_detected=false;
      if (count >= THRESHOLD) {
        nops_detected=true;
        reset_loop=true;
      }

      matched=scan_worker(write_buf, i, buf_len);
      if (matched > 0) {
          if (count == 0) {
            match_start=i;
          }
          count+=matched;
      } else {
        if (reset_loop == true) {
          printf("%p: %d consecutive NOPs found 🛷\n", addr_begin+match_start,count);
          reset_loop=false;

          write_buf = realloc(write_buf,count+1);
          memset(write_buf, '\0', count);
          lseek64(mem_fd,(unsigned long)(addr_begin+match_start),SEEK_SET);
          write(mem_fd,write_buf,count);

          count=0;
          i=match_start;

          write_buf = realloc(write_buf,buf_len);
          read(mem_fd,write_buf,buf_len);
          puts("Avalanche engaged, rescanning memory");
          goto retry;
        }
        count=0;
      }
    }
    if (reset_loop == true) { // catches the last iteration
      printf("%d consecutive NOPs found 🛷\n", count);
      reset_loop=false;
    }
    munlock(write_buf,buf_len);
  }


  if (nops_detected==false) {
    puts("No NOP sleds detected");
  } else {
    puts("WARNING: possible NOP sleds detected!");
  }


  free(read_buf);
  free(write_buf);
  fclose(fp);
  close(mem_fd);
}

void print_byte_len(size_t byte_len)
{
  if (byte_len > (1000^3))
    printf("Scanning %f gigabytes\n", (float) byte_len/(1000*1000*1000));
  else if (byte_len > (1000^2))
    printf("Scanning %f megabytes\n", (float) byte_len/(1000*1000));
  else if (byte_len > 1000)
    printf("Scanning %f kilobytes\n", (float) byte_len/1000);
  else
    printf("Scanning %lu bytes\n", byte_len);
}

void scan_mem()
{
  bool nops_detected=false, reset_loop=false, phase2=false;
  int i, count=0, match_start, matched;
  size_t byte_len;
  unsigned char *addr = NULL;

  byte_len = get_free_bytes();
  addr = malloc(byte_len);
  if (!addr) {
    fprintf(stderr, "Failed to allocate memory\n");
    exit(EXIT_FAILURE);
  }

  mlock(addr,byte_len);

#if defined(TESTING_MODE)
  puts("*** TESTING MODE ***");
  memset(addr, *nop_1b_1, THRESHOLD);
#endif
  print_byte_len(byte_len);

  for (i=0; i < byte_len; i++) {
retry:
    if (count >= THRESHOLD) {
      nops_detected=true;
      reset_loop=true;
    }

    matched=scan_worker(addr, i, byte_len);
    if (matched > 0) {
      if (reset_loop==false) {
        phase2=false;
        match_start=i;
      }
      count+=matched;
    } else {
      if (reset_loop == true) {
        printf("%d consecutive NOPs found 🛷\n", count);
        reset_loop=false;

        /* The null bytes, they're everywhere! */
        memset(addr+match_start,'\0',count);
        phase2=true;
        i=match_start;

        count=0;
        goto retry;
      }
      if (phase2 == true) {
        puts("Avalanche engaged, rescanning memory");
        phase2=false;
        nops_detected=false;
        count=0;
      }
    }
  }
  if (reset_loop == true) { // catches the last iteration
    printf("%d consecutive NOPs found 🛷\n", count);
    reset_loop=false;

    memset(addr+match_start,'\0',count);
    phase2=true;
    i=match_start;
    goto retry;
  }

  if (nops_detected==false) {
    puts("No NOP sleds detected");
  }

  munlock(addr,byte_len);

  free(addr);
}

int scan_worker(unsigned char *input_buf, int offset, size_t buf_len)
{
  int matched=0;
  unsigned char output_buf[MAX_NOP_LEN+1];

  memcpy(output_buf, &(input_buf[offset]), MAX_NOP_LEN);
  matched=scan_multibyte(output_buf);
  return matched;
}

/*
 * scan variable length bytes for nops
 */
int scan_multibyte(unsigned char *buffer)
{
  int rv=0;

  if (!memcmp(buffer,nop_11b_1,MAX_NOP_LEN))
    rv=MAX_NOP_LEN;
  else if (!memcmp(buffer,nop_10b_1,10)) {
    rv=10;
  } else if (!memcmp(buffer,nop_9b_1,9)) {
    rv=9;
  } else if (!memcmp(buffer,nop_8b_1,8)) {
    rv=8;
  } else if (!memcmp(buffer,nop_7b_1,7)) {
    rv=7;
  } else if (!memcmp(buffer,nop_6b_1,6)) {
    rv=6;
  } else if (!memcmp(buffer,nop_5b_1,5)) {
    rv=5;
  } else if (!memcmp(buffer,nop_4b_1,4)) {
    rv=4;
  } else if (!memcmp(buffer,nop_3b_1,3) ||
    !memcmp(buffer,nop_3b_2,3) ||
    !memcmp(buffer,nop_3b_3,3) ||
    !memcmp(buffer,nop_3b_4,3) ||
    !memcmp(buffer,nop_3b_5,3)) {
    rv=3;
  } else if (!memcmp(buffer,nop_2b_1,2) ||
      !memcmp(buffer,nop_2b_2,2) ||
      !memcmp(buffer,nop_2b_3,2) ||
      !memcmp(buffer,nop_2b_4,2)) {
    rv=2;
  } else if (!memcmp(buffer,nop_1b_1,1)) {
    rv=1;
  }

  return rv;
}

void print_help()
{
  puts("avalanche v2.0 🛷");
  puts("detect possible NOP sleds\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -m, --mem\t\tscan free memory");
  puts("  -p, --pid[=PID]\tscan process PID");
  puts("  -v, --verbose\t\tverbose output (PID scanning)");
}
