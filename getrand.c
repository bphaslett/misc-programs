/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * generate random ascii text using the system's prng
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * generate ten numbers between 1 and 20, average them out
 * the result should be somewhere in the middle
 * for i in {1..10}; do ./getrand -n 20; done | sed ':a;N;$!ba; s/\n/+/g; s,.*,(&)/10,g'|bc -l
 */

#include <stdio.h>    // for printf
#include <stdint.h>
#include <stdlib.h>   // for exit
#include <string.h>   // for strlen
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <ctype.h>    // for isalnum
#include <stdbool.h>

#define RAND_DEV "/dev/urandom"

static struct option long_options[] = {
  { "num",1, NULL, 'n' },
  { "alnum", 1, NULL, 'a' },
  { "hex", 1, NULL, 'H' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

enum DATA_TYPE_ {
  undef,
  alphanumeric,
  numeric,
  hexadecimal
} DATA_TYPE;

void print_help();
void get_random(size_t, unsigned int);

int main(int argc, char *argv[])
{
  int opt;
  size_t max_len=0;
  unsigned int numeric_range=0;

  if (argc < 2) { goto error; }

  while ((opt = getopt_long(argc, argv, "H:n:a:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'H':
        sscanf(optarg, "%lu", &max_len);
        DATA_TYPE=hexadecimal;
        break;
      case 'n': 
        sscanf(optarg, "%u", &numeric_range);
        max_len = strlen(optarg);
        DATA_TYPE=numeric;
        break;
      case 'a':
        sscanf(optarg, "%lu", &max_len);
        DATA_TYPE=alphanumeric;
        break;
      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
        break;
      default:
        print_help();
        return 0;
    }
  }

  if (DATA_TYPE > 0) {
    get_random(max_len, numeric_range);
    goto end;
  }

error:
  print_help();
  exit(EXIT_FAILURE);

end:
  return 0;
}

void get_random(size_t max_len, unsigned int numeric_range)
{
  bool non_zero = false;
  int i;
  unsigned int current_range = 0;
  char output_str[max_len+1], current_char;
  FILE *fp = fopen(RAND_DEV, "r");

  output_str[max_len]='\0';

  if (DATA_TYPE == alphanumeric) {
    for (i = 0; i < max_len; i++) {
      fread(&current_char, 1, 1, fp);
      do {
        // if it's not an alphanumeric character, try and turn it into one
        if (!isalnum(current_char))
          current_char &= 0x7F;   // mask is based on hex values of a-z A-Z 0-9

        if (!isalnum(current_char))
          current_char >>= 1;

        if (!isalnum(current_char))
          fread(&current_char, 1, 1, fp); // try again
      } while(!isalnum(current_char));
      printf("%c", current_char);
    }
  } else if (DATA_TYPE == numeric) {
    /*
     * start at the end of the array so that when we get to the first
     * digit, it's easier make sure we're not over numeric_range
     */
    i=max_len-1;
    while ((fread(&current_char, 1, 1, fp) > 0) && i >= 0) {
      // if it's not a digit, try and turn it into one
      if (!isdigit(current_char))
        current_char &= 0x3F;  // mask is based on hex value of characters 0-9

      if (!isdigit(current_char))
        current_char >>= 1;

      if (!isdigit(current_char))
        continue;

      output_str[i]=current_char;
      sscanf(output_str, "%u", &current_range);

      if (current_range <= numeric_range) {
        if (i == 0  && current_range == 0)
          continue;
        i--;
      }
    }

    // print it out, ignore leading zeros
    for (i=0; i < max_len; i++) {
      if (output_str[i] != '0' || non_zero == true) {
        non_zero = true;
        printf("%c", output_str[i]);
      }
    }
  } else if (DATA_TYPE == hexadecimal) {
    for (i = 0; i < max_len; i++) {
      fread(&current_char, 1, 1, fp);
      do {
        // if it's not an alphanumeric character, try and turn it into one
        if (!isxdigit(current_char))
          current_char &= 0x7F;   // mask is based on hex values of a-z A-Z 0-9

        if (!isxdigit(current_char))
          current_char >>= 1;

        if (!isxdigit(current_char))
          fread(&current_char, 1, 1, fp); // try again
      } while(!isxdigit(current_char));
      if (isupper(current_char)) {
        current_char = tolower(current_char);
      }
      if (isupper(current_char)) {
        current_char = tolower(current_char);
      }
      printf("%c", current_char);
    }
  }
  puts("");

  fclose(fp);
}

void print_help()
{
  puts("getrand");
  puts("get digits or alphanumeric characters from rng device\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -a, --alnum[=LENGTH]\t\toutput LENGTH alphanumeric characters");
  puts("  -n, --num[=MAX]\t\toutput a number between 1 and MAX");
  puts("  -H, --hex[=LENGTH]\t\toutput LENGTH hexadecimal characters");
  puts("  -h, --help\t\t\tyou are here");
}
