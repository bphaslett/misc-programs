/*
 * Copyright (C) 2019 Brian Haslett
 *
 * Simulate a series of coinflips based on data from /dev/random
 * and calculate percentages
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */


#include <stdio.h>
#include <stdlib.h> // exit status
#include <sys/random.h> // getrandom
#include <getopt.h>     // getopt, getopt_long, struct option
#include <errno.h>    // errnos

#define BYTE_SZ 8

static struct option long_options[] = {
  { "flips", 1, NULL, 'f' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

void flip_coin(int);
void print_help();

int main(int argc, char *argv[])
{
  int flips, opt;

  if (argc < 2) {
    print_help();
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "f:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'f':
        flips = atoi(optarg);
        break;

      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
        break;
      default:
        print_help();
        return 0;
    }
  }

  if (flips > 0) {
    flip_coin(flips);
  } else {
    print_help();
    return -EINVAL;
  }
}

void flip_coin(int flips)
{
  short bit;
  int i;
  unsigned int head_cnt=0, tail_cnt=0, tot_cnt=0;
  size_t byte;

  /* the byte loop */
  while (tot_cnt < flips) {
    getrandom(&byte, 1, GRND_RANDOM);
    /* the bit loop */
    for (i=0; i<BYTE_SZ && tot_cnt<flips; i++, tot_cnt++) {
      bit = byte & 1;
      byte >>= 1;
      (bit == 0) ? head_cnt++ : tail_cnt++;
    }
  }
  printf("%d/%d total flips\n", tot_cnt, flips);
  printf("%d heads (%f%%)\n", head_cnt, 100*((float)head_cnt/flips)); 
  printf("%d tails (%f%%)\n", tail_cnt, 100*((float)tail_cnt/flips)); 

}

void print_help()
{
  puts("coinflip");
  puts("simulate a series of coin flips\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -f, --flips[=FLIPS]\tnumber of flips");
}
