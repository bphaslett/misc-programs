#!/bin/bash

declare progs=(bluetoothctl)
for i in ${progs[@]}; do 
    if [[ ! -e $(which "$i") ]]; then
        echo "missing required program: $i"
        exit 1
    fi
done

coproc bluetoothctl
user_mac=0
user_dev=0
PS3="Please enter a selection: "

get_paired() {
  local output
  local input
  local count=0

  printf "Check for paired devices (y/n)? "
  read input

  if [[ $input == "y" ]]; then
    echo "Checking for paired devices.."
    echo "paired-devices" >&${COPROC[1]}
    read output <&${COPROC[0]} 
    while ! echo $output | grep '^Device' > /dev/null; do
      read -t1 output <&${COPROC[0]} 
    done

    while echo $output | grep '^Device'; do
      read -p "Choose this device (y/n)? " input
      if [[ $input == "y" ]]; then
        user_mac=$(echo $output | awk '{print $2}')
        user_dev=$(echo $output | awk '{$1="";$2="";print $0}' | sed 's/^\s\s//g')
        menu
        break
      fi
      read -t1 output <&${COPROC[0]} 
    done
  else
    echo "Entering scanning mode.."
    scanning
  fi
}
scanning() {
  local mac_buffer
  local sel=0
  local output

  echo "scan on" >&${COPROC[1]}

  while [[ $sel != "y" ]]; do
    read output <&${COPROC[0]} 
# until I can figure out a way to 'grep [NEW]' here, this is going to look uglier than I'd prefer
# I suspect the issue has something to do with how colors are encoded in bluetoothctl output
    while ! echo $output | grep -E "Device [[:xdigit:]]{2}:[[:xdigit:]]{2}" | grep -Ev ":[[:xdigit:]]{2} Connected|Paired|TxPower|RSSI|ServicesResolved|Modalias|UUIDs|ManufacturerData"; do  
      read output <&${COPROC[0]} 
      echo $output
    done
    mac_buffer=$(echo $output | sed 's/.*Device \([^\s]*\s\)\(.*\)/\1(\2)/g')
    read -p "Use this device $mac_buffer (y/n)? " sel
    user_mac=$(echo $mac_buffer | sed 's/ .*//g')
    user_dev=$(echo $mac_buffer | sed 's/[^\s]*\s(\(.*\))/\1/g')
  done

  echo "scan off" >&${COPROC[1]}

  menu
}

menu() {
  local sel=0
  local output
  
  printf "\nDEVICE: %s (%s)\n\n" ${user_mac} "${user_dev}"
  select sel in \
  "pair device" \
  "connect device" \
  "disconnect device" \
  "trust device" \
  "untrust device" \
  "remove device" \
  "scanning mode" \
  "quit"; do
    case "$REPLY" in
      "1")
        pair
        ;;
      "2")
        connect
        ;;
      "3")
        disconnect
        ;;
      "4")
        trust
        ;;
      "5")
        untrust
        ;;
      "6")
        remove
        ;;
      "7")
        scanning
        ;;
      "8")
        shutdown
        ;;
      *)
        printf "\nERROR, INVALID SELECTION\n\n"
        menu
        ;;
    esac
    break
  done
}

pair() {
  local output
  echo "pair ${user_mac}" >&${COPROC[1]}
  read output <&${COPROC[0]} 
  while ! echo $output | grep -e "${user_mac} Paired: yes" -e "Pairing successful" -e "Failed to pair.*AlreadyExists"; do  
    read output <&${COPROC[0]} 
    echo $output
  done

  menu
}

trust() {
  local output
  echo "trust ${user_mac}" >&${COPROC[1]}
  read output <&${COPROC[0]} 
  while ! echo $output | grep -e "${user_mac} Trusted: yes" -e "Changing ${user_mac} trust succeeded"; do  
    read output <&${COPROC[0]} 
    echo $output
  done

  menu
}

untrust() {
  local output
  echo "untrust ${user_mac}" >&${COPROC[1]}
  read output <&${COPROC[0]} 
  while ! echo $output | grep -e "${user_mac} Trusted: no" -e "Changing ${user_mac} untrust succeeded"; do  
    read output <&${COPROC[0]} 
    echo $output
  done

  menu
}

connect() {
  local output
  echo "connect ${user_mac}" >&${COPROC[1]}
  read output <&${COPROC[0]} 
  while ! echo $output | grep -e "${user_mac} Connected: yes" -e "Connection successful"; do  
    read output <&${COPROC[0]} 
    echo $output
  done

  menu
}

disconnect() {
  local output
  echo "disconnect ${user_mac}" >&${COPROC[1]}
  read output <&${COPROC[0]} 
  while ! echo $output | grep -e "${user_mac} Connected: no" -e "Successful disconnected"; do  
    read output <&${COPROC[0]} 
    echo $output
  done

  menu
}

remove() {
  local output
  echo "remove ${user_mac}" >&${COPROC[1]}
  read output <&${COPROC[0]} 
  while ! echo $output | grep -e "[DEL] Device ${user_mac}" -e "Device has been removed"; do  
    read output <&${COPROC[0]} 
    echo $output
  done

  menu
}

shutdown() {
  echo "scan off" >&${COPROC[1]}
  echo "exit" >&${COPROC[1]}
  exit
}

main() {
  get_paired
}

main

