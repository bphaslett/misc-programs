int root_check();
void print_byte_len(size_t);
size_t get_free_bytes();
void drop_caches();
void scan_mem();
void scan_proc(unsigned int);
void print_help();
int scan_worker(unsigned char*, int, size_t);
int scan_multibyte(unsigned char*);

/*
 * defines how many NOPs is a sled
 * this is essentially a sensitivity setting
 */
#define THRESHOLD 64

/*
 * for testing mem scanner
 */
//#define TESTING_MODE

#define PID_MAX_LEN 7
#define ADDR_MAX_LEN 12
#define MAX_NOP_LEN 11

/* a lot of these were borrowed from binutils' tc-i386.c */
static const unsigned char nop_1b_1[] =
  {0x90};   /* nop			*/
static const unsigned char nop_2b_1[] =
  {0x66,0x90};    /* xchg %ax,%ax		*/
static const unsigned char nop_2b_2[] =
  {0x0a,0x0a};    /* or (%edx), %%cl */
static const unsigned char nop_2b_3[] =
  {0x0c,0x00};    /* or $0x0, %%al */
static const unsigned char nop_2b_4[] =
  {0x9f,0x9e};    /* lahf; sahf */
static const unsigned char nop_3b_1[] =
  {0x80,0xcb,0x00};    /* or $0x0, %%bl */
static const unsigned char nop_3b_2[] =
  {0x80,0xcf,0x00};    /* or $0x0, %%cl */
static const unsigned char nop_3b_3[] =
  {0x80,0xca,0x00};    /* or $0x0, %%dl */
static const unsigned char nop_3b_4[] =
  {0x48,0x87,0xc0};    /* xchg %%rax, %%rax */
static const unsigned char nop_3b_5[] =
  {0x0f,0x1f,0x00}; /* nopl 0(%[re]ax) */
static const unsigned char nop_4b_1[] =
  {0x0f,0x1f,0x40,0x00};
/* nopl 0(%[re]ax,%[re]ax,1) */
static const unsigned char nop_5b_1[] =
  {0x0f,0x1f,0x44,0x00,0x00};
/* nopw 0(%[re]ax,%[re]ax,1) */
static const unsigned char nop_6b_1[] =
  {0x66,0x0f,0x1f,0x44,0x00,0x00};
/* nopl 0L(%[re]ax) */
static const unsigned char nop_7b_1[] =
  {0x0f,0x1f,0x80,0x00,0x00,0x00,0x00};
/* nopl 0L(%[re]ax,%[re]ax,1) */
static const unsigned char nop_8b_1[] =
  {0x0f,0x1f,0x84,0x00,0x00,0x00,0x00,0x00};
/* nopw 0L(%[re]ax,%[re]ax,1) */
static const unsigned char nop_9b_1[] =
  {0x66,0x0f,0x1f,0x84,0x00,0x00,0x00,0x00,0x00};
/* nopw %cs:0L(%[re]ax,%[re]ax,1) */
static const unsigned char nop_10b_1[] =
  {0x66,0x2e,0x0f,0x1f,0x84,0x00,0x00,0x00,0x00,0x00};
/* data16 nopw %cs:0L(%eax,%eax,1) */
static const unsigned char nop_11b_1[] =
  {0x66,0x66,0x2e,0x0f,0x1f,0x84,0x00,0x00,0x00,0x00,0x00};


static struct option long_options[] = {
  { "verbose", 0, NULL, 'v' },
  { "pid", 1, NULL, 'p' },
  { "mem", 0, NULL, 'm' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

