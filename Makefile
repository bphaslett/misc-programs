# variables
PREFIX=/usr/local
CFLAGS := -O0 -ggdb3
CXXFLAGS := ${CFLAGS}
CC := gcc
CXX := g++

SNEAKERS := sneakers
AVALANCHE := avalanche
C_TARGETS := scrub-free twist64 coinflip bpdb sorter getrand tpcalc
CXX_TARGETS := Getrand

ALL_TARGETS := $(C_TARGETS) $(CXX_TARGETS) $(SNEAKERS) $(AVALANCHE)
all: $(ALL_TARGETS) bpdb

bpdb: bpdb.c bpdb.h bpdb_crypt.c bpdb_crypt.h
	$(CC) $(CFLAGS) -c -o bpdb_crypt.o bpdb_crypt.c
	$(CC) $(CFLAGS) -o bpdb bpdb_crypt.o bpdb.c -lm

$(AVALANCHE):%:%.c %.h
	$(CC) $(CFLAGS) -o $@ $< -lm

$(SNEAKERS):%:%.c %.h
	$(CC) $(CFLAGS) -o $@ $< -lm

$(CXX_TARGETS):%:%.cc
	$(CXX) $(CXXFLAGS) -o $@ $<

(C_TARGETS):%:%.c %.h
	$(CC) $(CFLAGS) -o $@ $<

install:
	install -t $(PREFIX)/sbin scrub-free
	install -t $(PREFIX)/bin getrand
	install -t $(PREFIX)/bin Getrand
	install -t $(PREFIX)/sbin avalanche
	install -t $(PREFIX)/bin sneakers
	install -t $(PREFIX)/bin bpdb
	install -t $(PREFIX)/bin bph-bluetooth.sh

uninstall:
	rm $(PREFIX)/sbin/scrub-free
	rm $(PREFIX)/bin/Getrand
	rm $(PREFIX)/sbin/avalanche
	rm $(PREFIX)/bin/sneakers
	rm $(PREFIX)/bin/bpdb
	rm $(PREFIX)/bin bph-bluetooth.sh


clean: $(ALL_TARGETS)
	for p in $?; do \
	[ ! -e $$p ] || rm $$p; \
	done

