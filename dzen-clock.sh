#!/bin/bash
#
# uses bash, ps, cat, echo, date, upower, printf, grep, sed, awk, sleep, xrandr, dzen2

declare progs=(bash ps cat echo date upower printf grep sed awk sleep xrandr dzen2)
for i in ${progs[@]}; do 
    if [[ ! -e $(which "$i") ]]; then
        echo "missing required program: $i"
        exit 1
    fi
done

# the barSize X/Y may vary depending on which font you use
barSizeX=370
barSizeY=23

# update interval in seconds
updateTime=1

# normally xrandr is used to determine this, but if that doesn't work you can manually set e.g. 1280x720
#currentRes=$(xrandr | grep "\*" | awk '{print $1}')
currentRes=1366x768

# the name of the script you are currently looking at
scriptName=dzen-clock.sh
# the name of the program this script runs
cmdName=dzen2

# this adds a margin on the right for the "trayer" systray program
trayerSize=100

# battery warning threshhold as percent
thresh=15

# set to 1 to align to the bottom of the screen, default is top
alignBottom=1

# leave this alone; it's initialized here as a precaution
level=0

killOldPIDs() {
  killall -o 2s $scriptName 2>/dev/null
}

setResolution() {
  if [[ -z ${currentRes} ]]; then
      currentRes=$(xrandr --current | grep "\+" | grep -v "(" | awk '{print $1}')
  fi
  resX=$(echo $currentRes | cut -dx -f1)
  if [[ -n $trayerSize ]]; then
    barOffsetX=$resX-$trayerSize
  else
    barOffsetX=$resX
  fi
  (( barOffsetX-=$barSizeX ))
  if [[ ${alignBottom} == "1" ]]; then
      resY=$(echo $currentRes | cut -dx -f2)
      barOffsetY=$resY
      (( barOffsetY-=$barSizeY ))
  else
      barOffsetY=0
  fi

}

displayDzenClock() {
  while (true); do
  # battery warning
      level=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | cut -d: -f2 | sed -e 's/ \+//g' -e 's/%//g');
      if [[ ${level} -lt ${thresh} ]]; then
          notify-send -t 1000 "WARNING: LOW BATTERY"
      fi

  # the output
      echo $(date +"%a %m/%d/%y %H:%M:%S") \
      $(printf " ---- Battery: ") \
      $(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | cut -d: -f2 | sed -e 's/ \+//g');

      sleep $updateTime;
  done | dzen2 -x ${barOffsetX} -y ${barOffsetY} -w ${barSizeX} -h ${barSizeY} -u &
}

# if dzen2 is running, kill all old scripts and exit
if [[ -n $(ps faux | grep $cmdName | grep -v grep) ]]; then
  killOldPIDs
  exit 0
fi

killOldPIDs
setResolution
displayDzenClock
