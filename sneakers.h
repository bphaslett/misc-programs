/*
 * Copyright (C) 2017-2020 Brian Haslett
 *
 * encrypt or decrypt messages using various encryption algorithms
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdbool.h>

struct shared_s;
struct multi_s;
struct spiral_s;
struct rail_s;
struct caes_s;
struct vig_s;
struct sub_s;
struct bools_s;

void print_help();
void print_version();
int error_check(struct bools_s*, struct shared_s*);
int run_confirmation();
int parse_message(char *, struct shared_s*);
void print_output(struct shared_s*);
void encrypt_vigenere(char*, char*, size_t, char*, size_t);
void decrypt_vigenere(char*, char*, size_t, char*, size_t);
void encrypt_caesar(struct caes_s*);
void decrypt_caesar(struct caes_s*);
void run_substitution(struct sub_s*);
void spiral_down(struct spiral_s*, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool);
void spiral_right(struct spiral_s*, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool);
void spiral_up(struct spiral_s*, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool);
void spiral_left(struct spiral_s*, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool);
void init_spiral(struct spiral_s*, size_t, size_t);
void encrypt_spiral(struct spiral_s*);
void decrypt_spiral(struct spiral_s*);
void encrypt_rail(char*, char*, size_t, size_t);
void decrypt_rail(char*, char*, size_t, size_t);
void encrypt_multi(struct multi_s*);
void decrypt_multi(struct multi_s*);
void encrypt_multi2(struct multi_s*);
void decrypt_multi2(struct multi_s*);
void encrypt_multi3(struct multi_s*);
void decrypt_multi3(struct multi_s*);

void write_null_bytes(size_t, size_t x_len, int, char[][x_len+1]);
void array_to_string(size_t, size_t x_len, struct shared_s*, char output_text[][x_len+1]);
void string_to_array(size_t, size_t x_len, struct shared_s*, char[][x_len+1]);
int calc_remainders(size_t, size_t, size_t*);

enum __attribute__ ((__packed__)) _char_type {
  UNDEF,
  DIGIT,
  ALPHA,
} char_type;

struct shared_s {
  bool batch_mode;
  bool multi_mode;
  bool file_mode;
  size_t msg_len;
  char *msg_str;
  char *cipher_str;
};

struct sub_s {
  char sub_alphabet[27];
  struct shared_s *shared;
};

struct vig_s {
  size_t key_len;
  char *key;
  struct shared_s *shared;
};

struct caes_s {
  int shift;
  struct shared_s *shared;
};

struct spiral_s {
  int len_cnt;
  int x, y;
  int x_out, y_out;
  int xL, xR, yT, yB;
  struct shared_s *shared;
};

struct rail_s {
  size_t fence_height;
  struct shared_s *shared;
};

struct multi_s {
  int rounds;
  struct shared_s *shared;
  struct vig_s *vig;
  struct rail_s *rail;
  struct spiral_s *sp;
};

struct bools_s {
  bool do_encrypt;
  bool do_decrypt;
  bool has_key;
  bool has_shift;
  bool has_message;
  bool has_alg;
  bool has_sub;
  bool has_rounds;
  bool has_failed;
};
