/*
 * Copyright (C) 2019-2021 Brian Haslett
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h> // for malloc
#include <string.h>     // for memcpy
#include <stdbool.h>
#include <ctype.h> // for isdigit/isalpha/etc
#include <math.h> // for sqrt
#include <sys/mman.h>   // for mmap/munmap
#include <errno.h>
#include <termios.h>    // for tcgetattr/tcsetattr

#include "bpdb_crypt.h"
#include "bpdb.h"

enum __attribute__ ((__packed__)) _char_type {
  UNDEF,
  DIGIT,
  ALPHA,
} char_type;

/*
 * vigenere++, a polyalphanumeric substitution cipher
 *
 * input:
 * msg_str - message to encrypt
 * msg_len - length of message to encrypt
 * key - key/passphrase
 * key_len - length of key/passphrase
 *
 * output:
 * cipher_str - cipher text output
 */
void encrypt_vigenere(char *cipher_str, char *msg_str, size_t msg_len, char *key, size_t key_len)
{
  char key_char, msg_char, result;
  int type, key_loop_count, shift, cipher_str_count = 0;

  // shift msg_str according to key, repeatedly
  for (int msg_loop_count=0, key_loop_count=0;
     msg_loop_count < msg_len;
     msg_loop_count++, key_loop_count++) {

    if (key_len == key_loop_count)
      key_loop_count = 0;

    // current letters we are dealing with
    msg_char = msg_str[msg_loop_count];
    key_char = key[key_loop_count];

    if (isdigit(msg_char)) {
      type=DIGIT;
    } else if (isalpha(msg_char)) {
      type=ALPHA;
    } else {
      type=UNDEF;
    }

    // type refers to the msg_str characters
    switch (type) {
      case UNDEF:
        key_loop_count--;   // key doesn't increment for these
        cipher_str[cipher_str_count] = msg_char;
        cipher_str_count++;
        break;;
      case DIGIT:
        //  calculate shift, based on key case
        if (isupper(key_char)) shift = key_char-'A';
        else if (islower(key_char)) shift = key_char-'a';
        else if (isdigit(key_char)) shift = key_char-'0';
        else if (ispunct(key_char)) shift = key_char-'!';

        // handle shift wraparound
        while (shift > 10) shift -= 10;
        while (msg_char+shift > '9') msg_char-=10;

        result = msg_char+shift;
        cipher_str[cipher_str_count] = result;
        cipher_str_count++;

        break;;
      case ALPHA:
        if (isupper(msg_char)) {
           //  calculate shift, based on key case
          if (isupper(key_char)) shift = key_char-'A';
          else if (islower(key_char)) shift = key_char-'a';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          while ((msg_char+shift) > 'Z') msg_char-=26;

          result = msg_char+shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;

        } else if (islower(msg_char)) {
          //  calculate shift, based on key case
          if (islower(key_char)) shift = key_char-'a';
          else if (isupper(key_char)) shift = key_char-'A';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          while ((msg_char+shift) > 'z') msg_char-=26;

          result = msg_char+shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;

        }
        break;;
    }
  }
}
void decrypt_vigenere(char *cipher_str, char *msg_str, size_t msg_len, char *key, size_t key_len)
{
  char key_char, msg_char, result;
  int type, key_loop_count, shift, cipher_str_count = 0;

  // shift msg_str according to key, repeatedly
  for (int msg_loop_count=0, key_loop_count=0;
     msg_loop_count < msg_len;
     msg_loop_count++, key_loop_count++) {

    if (key_len == key_loop_count)
      key_loop_count = 0;

    // current letters we are dealing with
    msg_char = msg_str[msg_loop_count];
    key_char = key[key_loop_count];

    if (isdigit(msg_char)) {
      type=DIGIT;
    } else if (isalpha(msg_char)) {
      type=ALPHA;
    } else {
      type=UNDEF;
    }

    switch (type) {
      case UNDEF:
        key_loop_count--;   // key doesn't increment for these
        cipher_str[cipher_str_count] = msg_char;
        cipher_str_count++;
        break;;
      case DIGIT:
        if (isupper(key_char)) shift = key_char-'A';
        else if (islower(key_char)) shift = key_char-'a';
        else if (isdigit(key_char)) shift = key_char-'0';
        else if (ispunct(key_char)) shift = key_char-'!';

        // handle shift wraparound
        while (shift > 10) shift -= 10;
        if (msg_char-shift < '0') msg_char+=10;

        result = msg_char-shift;
        cipher_str[cipher_str_count] = result;
        cipher_str_count++;
        break;;
      case ALPHA:
        if (isupper(msg_char)) {
          //  calculate shift, based on key case
          if (isupper(key_char)) shift = key_char-'A';
          else if (islower(key_char)) shift = key_char-'a';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          if ((msg_char-shift) < 'A') msg_char+=26;

          result = msg_char-shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;
        } else if (islower(msg_char)) {
          //  calculate shift, based on key case
          if (isupper(key_char)) shift = key_char-'A';
          else if (islower(key_char)) shift = key_char-'a';
          else if (isdigit(key_char)) shift = key_char-'0';
          else if (ispunct(key_char)) shift = key_char-'!';

          // handle shift wraparound
          while (shift > 26) shift -= 26;
          if ((msg_char-shift) < 'a') msg_char+=26;

          result = msg_char-shift;
          cipher_str[cipher_str_count] = result;
          cipher_str_count++;
        }
        break;;
    }
  }
}

/*
 * shared: shared struct containing msg_len length and msg_str string input
 * y_len: column size for 2d array output
 * x_len: row size for 2d array output
 * output: 2d array to send output
 */
void string_to_array(size_t y_len, size_t x_len,
    struct shared_text *shared, char output[][x_len+1])
{
  for (int y=0, len_cnt=0; y < y_len && len_cnt < shared->msg_len; y++)
    for (int x=0; x < x_len && len_cnt < shared->msg_len; x++, len_cnt++)
      output[y][x] = shared->msg_str[len_cnt];
}


/*
 * input: 2d array to read input
 * y_len: column size for 2d array input
 * x_len: row size for 2d array input
 * shared: shared struct containing msg_len size and cipher_str string output
 */
void array_to_string(size_t y_len, size_t x_len,
    struct shared_text *shared, char input[][x_len+1])
{
  for (int y=0, len_cnt=0; y < y_len; y++)
    for (int x=0; x < x_len && len_cnt < shared->msg_len; x++, len_cnt++)
      shared->cipher_str[len_cnt] = input[y][x];
}

/*
 * y_len: column size
 * x_len: row size
 * rem: remainders in the last row
 * output: output string
 */
void write_null_bytes(size_t y_len, size_t x_len,
    int rem, char output[][x_len+1])
{
  for (int i=0; i<y_len; i++) {
    output[i][x_len]='\0';
  }
  if (rem != 0) {
    for (int i=x_len-rem; i <= x_len; i++) {
      output[y_len-1][i]='\0';
    }
  }
}
/*
 * adjust y_len as necessary
 *
 * y_len: column size
 * x_len: row size
 * msg_len: total size of message
 *
 * yB needs to be reinitialized after this is called
 * returns number of remainders in the last row
 */
int calc_remainders(size_t msg_len, size_t x_len, size_t *y_len)
{
  int rem = 0;

  while ((x_len * (*y_len)) < msg_len) {
    (*y_len)++;
  }
  if (msg_len - (x_len * (*y_len)) == 0)
    rem=0;
  else
    rem = (x_len * (*y_len)) - msg_len;
  return rem;
}
/*
 * the syntax for these spiral direction functions is pretty much the same
 *
 * sp:  main spiral struct containing most of the variables needed
 * x_len:  size of rows in I/O text
 * input_text:  the input text
 * output_text:  the output text
 * simulate:  simulation mode; used in first half of the decryption process
 *
 * if running in simulation mode, output_text, input_text, and x_len are not used
 */
void spiral_left(struct spiral *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->x >= sp->xL && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x];
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->x != sp->xL)
      sp->x--;
    else
      break;
  }
}
void spiral_up(struct spiral *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->y >= sp->yT && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x];
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->y != sp->yT)
      sp->y--;
    else
      break;
  }
}
void spiral_right(struct spiral *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->x <= sp->xR && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x];
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->x != sp->xR)
      sp->x++;
    else
      break;
  }
}
void spiral_down(struct spiral *sp, size_t x_len, char input_text[][x_len+1], char output_text[][x_len+1], bool simulate)
{
  while (sp->y <= sp->yB && sp->len_cnt != sp->shared->msg_len) {
    if (simulate == false) {
      output_text[sp->y_out][sp->x_out] = input_text[sp->y][sp->x];
      if (sp->x_out == x_len-1) {
        sp->x_out=0;
        sp->y_out++;
      } else {
        sp->x_out++;
      }
    }
    sp->len_cnt++;
    if (sp->y != sp->yB)
      sp->y++;
    else
      break;
  }
}
void init_spiral(struct spiral *sp, size_t x_len, size_t y_len)
{
  sp->len_cnt = 0;
  sp->xL = 0;
  sp->yT = 0;
  sp->x_out = 0;
  sp->y_out = 0;
  sp->xR = x_len - 1;
  sp->yB = y_len - 1;
}
/*
 * spiral, a transposition cipher
 *
 * it's mostly just used in multipass2 and multipass3 to help reduce collisions,
 * since it doesn't really take a key or anything
 *
 * x's left and right (xL/xR) change along with
 * y's top and bottom (yT/yB) as it spirals inward (encrypt) or outward (decrypt)
 */
void encrypt_spiral(struct spiral *sp)
{
  int rem = 0;
  size_t x_len = (int) sqrt(sp->shared->msg_len), y_len = x_len;

  init_spiral(sp, x_len, y_len);

  /* sqrt returns rounded figure, y_len may need adjusting */
  if ((sp->shared->msg_len - (x_len * y_len)) != 0) {
    rem = calc_remainders(sp->shared->msg_len, x_len, &y_len);
    sp->yB = y_len-1;
  }
  char input_text[y_len][x_len+1], output_text[y_len][x_len+1];

  write_null_bytes(y_len, x_len, rem, input_text);
  write_null_bytes(y_len, x_len, rem, output_text);
  string_to_array(y_len, x_len, sp->shared, input_text);

  if (rem != 0) {
    sp->x=x_len-rem-1;
    sp->y=y_len-1;
  } else {
    sp->x=x_len-1;
    sp->y=y_len-1;
  }

  /* the main loop */
  while (sp->len_cnt != sp->shared->msg_len) {
    spiral_left(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->yB--;
    sp->y--;

    spiral_up(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->xL++;
    sp->x++;

    spiral_right(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->yT++;
    sp->y++;

    spiral_down(sp, x_len, input_text, output_text, false);
    if (sp->len_cnt == sp->shared->msg_len)
      goto done;
    sp->xR--;
    sp->x--;
  }
done:

  array_to_string(y_len, x_len, sp->shared, output_text);
}
void decrypt_spiral(struct spiral *sp)
{
  int rem = 0;
  size_t x_len = (int) sqrt(sp->shared->msg_len), y_len = x_len;
  init_spiral(sp, x_len, y_len);

  /* sqrt returns rounded figure, y_len may need adjusting */
  if ((sp->shared->msg_len - (x_len * y_len)) != 0) {
    rem = calc_remainders(sp->shared->msg_len, x_len, &y_len);
    sp->yB = y_len-1;
  }
  char output_text[y_len][x_len+1];
  write_null_bytes(y_len, x_len, rem, output_text);

  if (rem != 0) {
    sp->x=x_len-rem-1;
    sp->y=y_len-1;
  } else {
    sp->x=x_len-1;
    sp->y=y_len-1;
  }

  /* simulate the main loop */
  while (sp->len_cnt != sp->shared->msg_len) {
    spiral_left(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto right;
    sp->yB--;
    sp->y--;

    spiral_up(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto down;
    sp->xL++;
    sp->x++;

    spiral_right(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto left;
    sp->yT++;
    sp->y++;

    spiral_down(sp, 0, NULL, NULL, true);
    if (sp->len_cnt == sp->shared->msg_len)
      goto up;
    sp->xR--;
    sp->x--;
  }

  /*
   * spiral outwards from the center
   * while reading the input cipher backwards
   *
   * simulation gives an entry based on when
   * len_cnt reaches msg_len
   */
  while (sp->len_cnt != 0) {
up:
    sp->yT--;
    while (sp->y >= sp->yT && sp->len_cnt != 0) {
        output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
        sp->len_cnt--;
        if (sp->y != sp->yT)
          sp->y--;
        else
          break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->x--;
left:
    sp->xL--;
    while (sp->x >= sp->xL && sp->len_cnt != 0) {
      output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
      sp->len_cnt--;
      if (sp->x != sp->xL)
        sp->x--;
      else
        break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->y++;
down:
    sp->yB++;
    while (sp->y <= sp->yB && sp->len_cnt != 0) {
      output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
      sp->len_cnt--;
      if (sp->y != sp->yB)
        sp->y++;
      else
        break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->x++;
right:
    sp->xR++;
    while (sp->x <= sp->xR && sp->len_cnt != 0) {
      output_text[sp->y][sp->x] = sp->shared->msg_str[sp->len_cnt-1];
      sp->len_cnt--;
      if (sp->x != sp->xR)
        sp->x++;
      else
        break;
    }
    if (sp->len_cnt == 0)
      goto done;
    sp->y--;
  }

done:
  array_to_string(y_len, x_len, sp->shared, output_text);
}

/*
 * rail-fence, a transposition cipher
 *
 * input:
 * msg_str - message to encrypt
 * msg_len - length of message to encrypt
 * fence_height - height of the rail-fence
 *
 * output:
 * cipher_str - cipher text output
 */
void encrypt_rail(char *cipher_str, char *msg_str, size_t msg_len, size_t fence_height)
{
  int rem, rem_count = 0;
  int num, num_no_rem;

  rem = msg_len % fence_height;
  num = msg_len/fence_height + (rem>0 ? 1 : 0);
  if (rem > 0) {
    num_no_rem = num-1;
  }
  char fence[fence_height][num];

  /*
   * store the plaintext in column-major order in a 2d array
   */
  for (int x=0, z=0; x < num; x++) {
    for (int y=0; y < fence_height && z < msg_len; y++, z++) {
      fence[y][x] = msg_str[z];
    }
  }

  /*
   * read back the cipher_str in row-major order
   */
  for (int y=0, z=0; y < fence_height; y++) {
    for (int x=0; x < num && z < msg_len; x++, z++) {
      cipher_str[z] = fence[y][x];
    }
    if (rem > 0 && rem_count != rem) {
      rem_count++;
      if (rem_count == rem) {
        num = num_no_rem;
      }
    }
  }
}
void decrypt_rail(char *cipher_str, char *msg_str, size_t msg_len, size_t fence_height)
{
  int rem, rem_count = 0;
  int num, num_no_rem;

  rem = msg_len % fence_height;
  num = msg_len/fence_height + (rem>0 ? 1 : 0);
  if (rem > 0) {
    num_no_rem = num-1;
  }
  char fence[fence_height][num+1];

  /*
   * store the cipher_str in row-major order in a 2d array
   */
  for (int y=0, z=0; y < fence_height; y++) {
    for (int x=0; x < num && z < msg_len; x++, z++) {
      fence[y][x] = msg_str[z];
    }
    if (rem > 0 && rem_count != rem) {
      rem_count++;
      if (rem_count == rem) {
        num = num_no_rem;
      }
    }
  }

  /*
   * read back the plaintext in column-major order
   */
  if (rem > 0) {
    num++;
  }
  for (int x=0, z=0; x < num; x++) {
    for (int y=0; y < fence_height && z < msg_len; y++) {
      cipher_str[z] = fence[y][x];
      z++;
    }
  }
}

/*
 * multipass3, a combination of substitution and transposition ciphers
 * the key generates its own cipher which in turn is used to encrypt the message
 *
 * set the key and number of rounds
 */
void encrypt_multi3(struct multipass *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  char *key_cipher = mmap(NULL, multi->vig->key_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  char *key_buf = mmap(NULL, multi->vig->key_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK2(key_cipher, key_buf);
  memcpy(key_cipher, multi->vig->key, multi->vig->key_len);

  for (int i=0; i<rounds; i++) {
    /* encrypt the key */
    encrypt_vigenere(key_buf, key_cipher, multi->vig->key_len, multi->vig->key, multi->vig->key_len);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
    encrypt_rail(key_buf, key_cipher, multi->vig->key_len, multi->rail->fence_height/2);
    memcpy(key_cipher, key_buf, multi->vig->key_len);

    /* encrypt the message */
    encrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, key_cipher, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    encrypt_spiral(multi->sp);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
  }

  munmap(key_cipher, multi->vig->key_len+1);
  munmap(key_buf, multi->vig->key_len+1);
}
void decrypt_multi3(struct multipass *multi)
{
  int rounds = multi->rounds;
  size_t msg_len = multi->shared->msg_len;
  multi->rail->fence_height = multi->vig->key_len+1;
  char *key_cipher = mmap(NULL, multi->vig->key_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  char *key_buf = mmap(NULL, multi->vig->key_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK2(key_cipher, key_buf);
  memcpy(key_cipher, multi->vig->key, multi->vig->key_len);

  /* simulate the key encryption, and work backwards */
  for (int i=0; i<rounds; i++) {
    encrypt_vigenere(key_buf, key_cipher, multi->vig->key_len, multi->vig->key, multi->vig->key_len);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
    encrypt_rail(key_buf, key_cipher, multi->vig->key_len, multi->rail->fence_height/2);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
  }
  for (int i=0; i<rounds; i++) {
  /* decrypt the message */
    decrypt_spiral(multi->sp);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_rail(multi->shared->cipher_str, multi->shared->msg_str, msg_len, multi->rail->fence_height);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);
    decrypt_vigenere(multi->shared->cipher_str, multi->shared->msg_str, msg_len, key_cipher, multi->vig->key_len);
    memcpy(multi->shared->msg_str, multi->shared->cipher_str, msg_len);

    /* decrypt the key */
    decrypt_rail(key_buf, key_cipher, multi->vig->key_len, multi->rail->fence_height/2);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
    decrypt_vigenere(key_buf, key_cipher, multi->vig->key_len, multi->vig->key, multi->vig->key_len);
    memcpy(key_cipher, key_buf, multi->vig->key_len);
  }

  munmap(key_cipher, multi->vig->key_len+1);
  munmap(key_buf, multi->vig->key_len+1);
}

/*
 * multi->vig->key has to be freed eventually, but in cases where we're
 * decrypting, changing something, then reencrypting, make sure you don't free
 * it too soon
 *
 * set_crypt_info and get_crypt_info work similarly, although set_crypt_info
 * has a confirmation dialog
 */
int set_crypt_info(struct multipass *multi)
{
  bool echo = false;
  int rv;
  size_t map_len1, map_len2;
  char *input_buf1 = NULL, *input_buf2 = NULL;

  if (multi->set == true) {
    return 0;
  }

  printf("Number of rounds: ");
  map_len1 = parse_input(&input_buf1);
  if (map_len1 == 0) {
    errno = EINVAL;
    return -1;
  }

  multi->rounds = atoi(input_buf1);

  rv = echo_off(&echo);
  if (rv != 0)
    return -1;

retry:
  munmap(input_buf1, map_len1);

  printf("Password: ");
  map_len1 = parse_input(&input_buf1);
  puts("");
  if (map_len1 == 0) {
    errno = EINVAL;
    return -1;
  }

  printf("Password (confirmation): ");
  map_len2 = parse_input(&input_buf2);
  puts("");
  if (map_len2 == 0) {
    errno = EINVAL;
    return -1;
  }

  if (strcmp(input_buf1, input_buf2) != 0)
    goto retry;

  if (echo == true)
    echo_on();

  munmap(input_buf2, map_len2);

  multi->vig->key_len = strlen(input_buf1);
  multi->vig->key = mmap(NULL, multi->vig->key_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  MMAP_CHECK(multi->vig->key);
  strcpy(multi->vig->key, input_buf1);
  mprotect(multi->vig->key, multi->vig->key_len+1, PROT_READ);

  munmap(input_buf1, map_len1);

  multi->set = true;
  return 0;
}
int get_crypt_info(struct multipass *multi)
{
  bool echo = false;
  int rv;
  size_t map_len;
  char *input_buf = NULL;

  if (multi->set == true) {
    return 0;
  }

  printf("Number of rounds: ");
  map_len = parse_input(&input_buf);
  if (map_len == 0) {
    errno = EINVAL;
    return -1;
  }

  multi->rounds = atoi(input_buf);
  munmap(input_buf, map_len);

  rv = echo_off(&echo);
  if (rv != 0)
    return -1;

  printf("Password: ");
  map_len = parse_input(&input_buf);
  puts("");
  if (map_len == 0) {
    errno = EINVAL;
    return -1;
  }

  if (echo == true)
    echo_on();

  multi->vig->key_len = strlen(input_buf);
  multi->vig->key = mmap(NULL, multi->vig->key_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  MMAP_CHECK(multi->vig->key);
  strcpy(multi->vig->key, input_buf);
  mprotect(multi->vig->key, multi->vig->key_len+1, PROT_READ);

  munmap(input_buf, map_len);

  multi->set = true;
  return 0;
}

int echo_off(bool *echo)
{
  struct termios old, new;

  if (tcgetattr(fileno(stdin), &old) != 0)
    return -1;

  new = old;
  if (new.c_lflag & ECHO)
    *echo = true;
  else
    return 0;

  new.c_lflag &= ~ECHO;

  if (tcsetattr(fileno(stdin), TCSAFLUSH, &new) != 0)
    return -1;

  return 0;
}
int echo_on()
{
  struct termios old, new;

  if (tcgetattr(fileno(stdin), &old) != 0)
    return -1;

  new = old;
  new.c_lflag |= ECHO;

  /* Restore terminal. */
  (void) tcsetattr (fileno (stdin), TCSAFLUSH, &new);

  return 0;
}

/*
 * encrypt/decrypt a file with multipass3
 *
 * key, rounds, and msg_str must be set prior
 *
 * since it's really just the file we're working with for now
 * memory is freed right away
 */
int encrypt_file_multi3(struct multipass *multi, FILE *fp)
{
  multi->shared->msg_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_SHARED, fileno(fp), 0);
  multi->shared->cipher_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK2(multi->shared->msg_str, multi->shared->cipher_str);

  encrypt_multi3(multi);

  munmap(multi->shared->msg_str, multi->shared->msg_len+1);
  munmap(multi->shared->cipher_str, multi->shared->msg_len+1);

  return 0;
}
int decrypt_file_multi3(struct multipass *multi, FILE *fp)
{
  multi->shared->msg_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_SHARED, fileno(fp), 0);
  multi->shared->cipher_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK2(multi->shared->msg_str, multi->shared->cipher_str);

  decrypt_multi3(multi);

  munmap(multi->shared->msg_str, multi->shared->msg_len+1);
  munmap(multi->shared->cipher_str, multi->shared->msg_len+1);

  return 0;
}

int encrypt_mem(struct multipass *multi, char *buf)
{
  multi->shared->msg_len = strlen(buf);
  multi->shared->msg_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  multi->shared->cipher_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK2(multi->shared->msg_str, multi->shared->cipher_str);

  memcpy(multi->shared->msg_str, buf, multi->shared->msg_len);
  encrypt_multi3(multi);
  multi->shared->msg_str[multi->shared->msg_len] = '\0';
  memcpy(buf, multi->shared->msg_str, multi->shared->msg_len);

  munmap(multi->shared->msg_str, multi->shared->msg_len+1);
  munmap(multi->shared->cipher_str, multi->shared->msg_len+1);

  return 0;
}
int decrypt_mem(struct multipass *multi, FILE *fp)
{
  char *buf = mmap(NULL, multi->shared->msg_len+1, PROT_READ, MAP_PRIVATE, fileno(fp), 0);
  multi->shared->msg_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);
  multi->shared->cipher_str = mmap(NULL, multi->shared->msg_len+1, PROT_READ|PROT_WRITE, MAP_ANON|MAP_PRIVATE, 0, 0);

  MMAP_CHECK3(buf, multi->shared->msg_str, multi->shared->cipher_str);

  memcpy(multi->shared->msg_str, buf, multi->shared->msg_len+1);
  munmap(buf, multi->shared->msg_len+1);

  decrypt_multi3(multi);
  multi->shared->msg_str[multi->shared->msg_len] = '\0';

  munmap(buf, multi->shared->msg_len+1);
  munmap(multi->shared->cipher_str, multi->shared->msg_len+1);

  return 0;
}
