/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * twist -- generate twist table from kernel's drivers/char/random.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf, sscanf
#include <stdint.h>
#include <stdlib.h>   // for exit
#include <string.h>   // for strlen
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <errno.h>
#include <stdbool.h>

#define rol64(word, shift)  ({ \
  uint64_t _word = word; \
  __asm__ __volatile__ ("rolq %%cl, %%rax;" : "=a" (_word) : "a" (_word), "c" (shift) : ); \
  _word; })

#define ror64(word, shift) ({ \
  uint64_t _word = word; \
  __asm__ __volatile__ ("rorq %%cl, %%rax;" : "=a" (_word) : "a" (_word), "c" (shift) : ); \
  _word; \
})

static struct option long_options[] = {
  { "decimal", 0, NULL, 'd' },
  { "hexadecimal", 0, NULL, 'H' },
  { "iterations", 1, NULL, 'i' },
  { "value1", 1, NULL, 'a' },
  { "value2", 1, NULL, 'b' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

enum format_types {
  HEX,
  DEC
};

struct shared_s {
  int iterations;
  uint64_t value1;
  uint64_t value2;
};

void print_help();
void twist_table(struct shared_s *shared);

int main(int argc, char *argv[])
{
  bool has_a = false, has_b = false;
  int opt;
  enum format_types format;
  char *str1, *str2;
  struct shared_s *shared = malloc(sizeof(*shared));

  if (!shared) {
    fprintf(stderr, "Error allocating memory in main (shared)\n");
    return -ENOMEM;
  }

  format=HEX;
  shared->iterations=1;

  if (argc < 2) { goto error; }

  while ((opt = getopt_long(argc, argv, "dHi:a:b:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'd':
        format=DEC;
        break;
      case 'a':
        str1 = malloc(strlen(optarg)+1);
        if (!str1) {
          fprintf(stderr, "Error allocating memory in main (str1)\n");
          if (str2) { free(str2); }
          free(shared);
          return -ENOMEM;
        }

        strcpy(str1, optarg);
        has_a = true;
        break;
      case 'b':
        str2 = malloc(sizeof(optarg)+1);
        if (!str2) {
          fprintf(stderr, "Error allocating memory in main (str2)\n");
          if (str1) { free(str1); }
          free(shared);
          return -ENOMEM;
        }

         strcpy(str2, optarg);
        has_b = true;
        break;
      case 'i':
        shared->iterations = atoi(optarg);
        break;
      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
      default:
        print_help();
        return 0;
    }
  }

  if (has_a == true) {
    switch (format) {
      case DEC: 
        sscanf(str1, "%lu", &shared->value1);
        break;
      case HEX:
        sscanf(str1, "%lx", &shared->value1);
        break;
    }

    if (has_b == true) {
      switch (format) {
        case DEC:
          sscanf(str2, "%lu", &shared->value2);
          break;
        case HEX:
          sscanf(str2, "%lx", &shared->value2);
          break;
      }

      twist_table(shared);
      goto end;
    }
  }

error:
  print_help();
  free(shared);
  exit(EXIT_FAILURE);

end:
  free(shared);
  free(str1);
  free(str2);
  return 0;
}

/*
 * COLUMN3a = COLUMN1a ^ COLUMN2a
 * COLUMN4a = COLUMN3a; rol64(COLUMN4a, 3)
 * COLUMN1b = ~COLUMN4a
 * COLUMN2b = COLUMN2a; ror64(COLUMN2b, 1)
 */
void twist_table(struct shared_s *shared)
{
  int n;
  uint64_t value1a = shared->value1;
  uint64_t value2a = shared->value2;
  uint64_t value3a = value1a ^ value2a;
  uint64_t value4a = rol64(value3a, 3);

  printf("{ 0x%.16lX, 0x%.16lX, 0x%.16lX, 0x%.16lX },\n",
          value1a, value2a, value3a, value4a);

  /* begin loop */
  for (n=0; n < shared->iterations-1; n++) {
    value1a = ~value4a;
    value2a = ror64(value2a, 1);
    value3a = value1a ^ value2a;
    value4a = rol64(value3a, 3);

    printf("{ 0x%.16lX, 0x%.16lX, 0x%.16lX, 0x%.16lX },\n",
            value1a, value2a, value3a, value4a);
  }

}

void print_help()
{
  puts("twist");
  puts("generate twist table\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -a, --value1\t\tvalue 1 (required)");
  puts("  -b, --value2\t\tvalue 2 (required)");
  puts("  -i, --iterations\tnumber of iterations");
  puts("  -d, --decimal\t\tinput values are in base-10");
  puts("  -H, --hexadecimal\tinput values are in base-16 (default)");
}

