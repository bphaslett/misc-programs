/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * generate random ascii text using the system's prng
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * generate ten numbers between 1 and 20, average them out
 * the result should be somewhere in the middle
 * for i in {1..10}; do ./Getrand -n 20; done | sed ':a;N;$!ba; s/\n/+/g; s,.*,(&)/10,g'|bc -l
 */

#include <iostream>   // for cout/cerr/etc..
#include <cstdint>    // for int types
#include <cstdlib>    // for exit
#include <cstring>    // for strlen
#include <cctype>     // for isalnum
#include <getopt.h>   // for getopt, getopt_long, struct option

#define RAND_DEV "/dev/urandom"

static struct option long_options[] = {
  { "hex", 1, NULL, 'H' },
  { "num", 1, NULL, 'n' },
  { "alnum", 1, NULL, 'a' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

enum DATA_TYPE_ {
  undef,
  alphanumeric,
  numeric,
  hexadecimal
} DATA_TYPE;

class CommonStorage {
  private:
  char current_char_;
  bool non_zero_;
  unsigned int numeric_range_;   // numeric
  unsigned int current_range_;
  unsigned long max_length_;   // numeric, alphanumeric
  FILE *fp_;
  void GetRandom();

  public:
  void PrintHelp();

  CommonStorage() {
    non_zero_=false;
    current_range_=0;
    fp_ = fopen(RAND_DEV, "r");
  }
  ~CommonStorage() {
    fclose(fp_);
  }

  int HandleSelection()
  {
    if (DATA_TYPE > 0) {
      GetRandom();
      return 0;
    } else {
      PrintHelp();
      return -EINVAL;
    }
  }
  void SetNumeric(char *option)
  {
    sscanf(option, "%u", &numeric_range_);
    max_length_ = strlen(option);
    DATA_TYPE = numeric;
  }
  void SetAlphanumeric(char *option)
  {
    sscanf(option, "%lu", &max_length_);
    DATA_TYPE = alphanumeric;
  }
  void SetHexadecimal(char *option)
  {
    sscanf(option, "%lu", &max_length_);
    DATA_TYPE = hexadecimal;
  }
};

int main(int argc, char *argv[])
{
  CommonStorage storage;
  int opt, ret;

  while ((opt = getopt_long(argc, argv, "H:n:a:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'H':
        storage.SetHexadecimal(optarg);
        break;
      case 'n': 
        storage.SetNumeric(optarg);
        break;
      case 'a':
        storage.SetAlphanumeric(optarg);
        break;
      case 'h':
        storage.PrintHelp();
        exit(EXIT_SUCCESS);
        break;
    }
  }

  return storage.HandleSelection();
}

void
CommonStorage::GetRandom()
{
  int i;
  char out_str[max_length_+1];

  out_str[max_length_] = '\0';

  if (DATA_TYPE == alphanumeric) {
    for (i = 0; i < max_length_; i++) {
      fread(&current_char_, 1, 1, fp_);
      do {
        // if it's not an alphanumeric character, try and turn it into one
        if (!isalnum(current_char_))
          current_char_ &= 0x7F;   // mask is based on hex values of a-z A-Z 0-9

        if (!isalnum(current_char_))
          current_char_ >>= 1;

        if (!isalnum(current_char_))
          fread(&current_char_, 1, 1, fp_); // try again
      } while(!isalnum(current_char_));
      std::cout << current_char_;
    }
  } else if (DATA_TYPE == numeric) {
retry:
    /*
     * start at the end of the array so that when we get to the first
     * digit, it's easier make sure we're not over numeric_range_
     */
    i=max_length_-1;
    while ((fread(&current_char_, 1, 1, fp_) > 0) && i >= 0) {
      // if it's not a digit, try and turn it into one
      if (!isdigit(current_char_))
        current_char_ &= 0x3F;  // mask is based on hex value of characters 0-9

      if (!isdigit(current_char_))
        current_char_ >>= 1;

      if (!isdigit(current_char_))
        continue;

      out_str[i]=current_char_;
      sscanf(out_str, "%u", &current_range_);

      if (current_range_ <= numeric_range_) {
        if (i == 0  && current_range_ == 0)
          continue;
        i--;
      }
    }

    // print it out, ignore leading zeros
    for (i=0; i<max_length_; i++) {
      if (out_str[i] != '0' || non_zero_ == true) {
        non_zero_ = true;
        std::cout << out_str[i];
      }
    }
  } else if (DATA_TYPE == hexadecimal) {
    for (i = 0; i < max_length_; i++) {
      fread(&current_char_, 1, 1, fp_);
      do {
        // if it's not a hexadecimal character, try and turn it into one
        if (!isxdigit(current_char_))
          current_char_ &= 0x7F;   // mask is based on hex values of a-z A-Z 0-9

        if (!isxdigit(current_char_))
          current_char_ >>= 1;

        if (!isxdigit(current_char_))
          fread(&current_char_, 1, 1, fp_); // try again
      } while(!isxdigit(current_char_));
      if (isupper(current_char_)) {
        current_char_ = tolower(current_char_);
      }
      std::cout << current_char_;
    }
  }
  std::cout << "\n";
}

void
CommonStorage::PrintHelp()
{
  std::cout << "getrand\n";
  std::cout << "get digits or alphanumeric characters from rng device\n\n";
  std::cout << "Usage: [options]\n\n";
  std::cout << "Options:\n";
  std::cout << "  -a, --alnum[=LENGTH]\t\toutput LENGTH alphanumeric characters\n";
  std::cout << "  -n, --num[=MAX]\t\toutput a number between 1 and MAX\n";
  std::cout << "  -H, --hex[=LENGTH]\t\toutput LENGTH hexadecimal characters\n";
  std::cout << "  -h, --help\t\t\tyou are here\n";
}
